#include <octopus/threading/thread.hpp>


namespace octo {

static const u16 thread_id = 0;

OCTO_PUBLIC
u16 thread_get_id() {
	return thread_id;
}

}
