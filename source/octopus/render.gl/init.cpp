#include "init.hpp"
#include "impl.hpp"
#include "device.hpp"
#include <octopus/memory/allocator.hpp>
#include <octopus/log/log.hpp>

namespace octo::render {

#if OCTO_OPENGL_DEBUG_LOG

static StringRef gl_from_source(GLenum source) {
	switch (source) {
	case GL_DEBUG_SOURCE_API: return "api";
	case GL_DEBUG_SOURCE_APPLICATION: return "app";
	case GL_DEBUG_SOURCE_OTHER: return "other";
	case GL_DEBUG_SOURCE_SHADER_COMPILER: return "shader";
	case GL_DEBUG_SOURCE_THIRD_PARTY: return "3party";
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM: return "ws";
	default: octo_unreachable;
	}
}

static StringRef gl_from_type(GLenum source) {
	switch (source) {
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "deprecated";
	case GL_DEBUG_TYPE_ERROR: return "error";
	case GL_DEBUG_TYPE_PERFORMANCE: return "perf";
	case GL_DEBUG_TYPE_PORTABILITY: return "portability";
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "ub";
	case GL_DEBUG_TYPE_OTHER: return "other";
	default: octo_unreachable;
	}
}

static StringRef gl_from_severity(GLenum severity) {
	switch (severity) {
	case GL_DEBUG_SEVERITY_NOTIFICATION: return "note";
	case GL_DEBUG_SEVERITY_LOW: return "low";
	case GL_DEBUG_SEVERITY_MEDIUM: return "med";
	case GL_DEBUG_SEVERITY_HIGH: return "high";
	default: octo_unreachable;
	}
}

static void application_glfw_openg_debug_proc(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam
) {
#define PARAMS ( \
	"GL $src: [$id] $msg ($type/$sev)", \
	{"id", id}, \
	{"src", gl_from_source(source)}, \
	{"msg", message}, \
	{"type", gl_from_type(type)}, \
	{"sev", gl_from_severity(severity)} \
	)

	switch (type) {
	case GL_DEBUG_TYPE_OTHER: {
		OCTO_APPLY(LOGv, PARAMS);
	} break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
	case GL_DEBUG_TYPE_PORTABILITY:
	case GL_DEBUG_TYPE_PERFORMANCE: {
		OCTO_APPLY(LOGw, PARAMS);
	} break;
	case GL_DEBUG_TYPE_ERROR:
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: {
		OCTO_APPLY(LOGe, PARAMS);
	} break;
	default: {
		// skip everything else
	} break;
	}

#undef PARAMS

}

#endif

Context* context_create() {
	Context* context = OCTO_RENDER_CREATE_OBJECT(Context);
#if OCTO_OPENGL_DEBUG_LOG
	GL_CHECK(glEnable(GL_DEBUG_OUTPUT));
	#if OCTO_OPENGL_DEBUG_LOG_SYNCHRONOUS
	GL_CHECK(glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS));
	#endif
	GL_CHECK(glDebugMessageCallback(application_glfw_openg_debug_proc, nullptr));
#endif
	return context;
}

void context_destroy(Context* context) {
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(context);
	OCTO_RENDER_DESTROY_OBJECT(context);
}

Device* device_create(Context* context) {
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(context);

	Device* device = OCTO_RENDER_CREATE_OBJECT(Device);
	device->context = context;
	return device;
}

void device_destroy(Device* device) {
	OCTO_RENDER_DESTROY_OBJECT(device);
}

}
