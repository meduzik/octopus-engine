#pragma once

#include <octopus/render/device.hpp>
#include "checks.hpp"

namespace octo::render {

inline constexpr uword MaxBufferSize = 256 * 1024 * 1024;
inline constexpr uword MaxShaderSourceLength = 1 * 1024 * 1024;

struct Context {
	OCTO_RENDER_OBJECT(Context);

};

struct Device {
	OCTO_RENDER_OBJECT(Device);

	std::array<GLuint, 2> buffers;

	Context* context;
};

}
