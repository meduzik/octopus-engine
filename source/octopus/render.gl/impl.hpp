#pragma once

#include "opengl.hpp"
#include <octopus/core/types.hpp>
#include <octopus/log/log.hpp>
#include "device.hpp"

#if OCTO_OPENGL_SYNC_ERROR_CHECK
namespace octo::render {
	void gl_check(const char* location, const char* func, const char* call);

	template<class Fn>
	std::enable_if_t<
		std::is_same_v<decltype(std::declval<Fn>()()), void>,
		void
	> gl_checked_call(Fn&& fn, const char* location, const char* func, const char* call) {
		fn();
		gl_check(location, func, call);
	}

	template<class Fn>
	std::enable_if_t<
		!std::is_same_v<decltype(std::declval<Fn>()()), void>,
		decltype(std::declval<Fn>()())
	> gl_checked_call(Fn&& fn, const char* location, const char* func, const char* call) {
		auto res = fn();
		gl_check(location, func, call);
		return res;
	}
}

	#define GL_CHECK(fn) gl_checked_call([&]() { return (fn); }, OCTO_LOG_LOCATION, OCTO_PRETTY_FUNCTION, #fn)
#else
	#define GL_CHECK(fn) fn
#endif

#define OCTO_RENDER_CREATE_OBJECT(T, ...) Mem.create<T>(__VA_ARGS__)

#if OCTO_RENDER_CHECKS
	#define OCTO_RENDER_CHECK_NOT_NULL(e) octo_require(e != nullptr)
	#define OCTO_RENDER_CHECK(e) octo_require(e)
	#define OCTO_RENDER_CHECK_OBJECT_OR_NULL(e) octo_require(e->tag == decltype(*e)::Tag)
	#define OCTO_RENDER_CHECK_OBJECT_NOT_NULL(e) octo_require((e == nullptr) || (e->tag == std::remove_reference_t<decltype(*e)>::Tag))

	#define OCTO_RENDER_DESTROY_OBJECT(e) do{e->tag = RenderObjectTag::Invalid; Mem.destroy(e); e = nullptr;}while(false)
#else
	#define OCTO_RENDER_CHECK_NOT_NULL(e)
	#define OCTO_RENDER_CHECK(e)
	#define OCTO_RENDER_CHECK_OBJECT_OR_NULL(e)
	#define OCTO_RENDER_CHECK_OBJECT_NOT_NULL(e)

	#define OCTO_RENDER_DESTROY_OBJECT(e) Mem.destroy(e)
#endif

namespace octo::render {

}



