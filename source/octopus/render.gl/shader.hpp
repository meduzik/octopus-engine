#pragma once

#include <octopus/render/shader.hpp>
#include "checks.hpp"
#include "impl.hpp"

namespace octo::render {

struct Shader {
	OCTO_RENDER_OBJECT(Shader);
	GLuint name;
#if OCTO_RENDER_CHECKS
	ShaderType type;
#endif
};

}
