#include "shader.hpp"


namespace octo::render {

GLenum gl_shader_type_to_type[] = {
	GL_VERTEX_SHADER,
	GL_FRAGMENT_SHADER
};

#if OCTO_RENDER_CHECKS
bool IsValidShaderType(ShaderType type) {
	return type == ShaderType::Vertex || type == ShaderType::Fragment;
}
#endif

OCTO_PUBLIC
Shader* shader_compile(Device* device, ShaderType type, StringRef source) {
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(device);
	OCTO_RENDER_CHECK(IsValidShaderType(type));
	OCTO_RENDER_CHECK(source.size() > 0);
	OCTO_RENDER_CHECK(source.size() < MaxShaderSourceLength);

	GLuint name = GL_CHECK(glCreateShader(gl_shader_type_to_type[(i32)type]));
	if (!name) {
		LOGw("Failed to allocate OpenGL shader name");
		return nullptr;
	}

	const GLchar* gl_string = (const GLchar*)source.data();
	GLint gl_length = (GLint)source.size();

	GL_CHECK(glShaderSource(name, 1, &gl_string, &gl_length));

	Shader* shader = OCTO_RENDER_CREATE_OBJECT(Shader);
	shader->name = name;
#if OCTO_RENDER_CHECKS
	shader->type = type;
#endif
	return shader;
}

OCTO_PUBLIC
void shader_destroy(Device* device, Shader* shader) {
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(device);
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(shader);
	GL_CHECK(glDeleteShader(shader->name));
	OCTO_RENDER_DESTROY_OBJECT(shader);
}

OCTO_PUBLIC
bool shader_query_status(Device* device, Shader* shader, Arena* arena, StringRef* message) {
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(device);
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(shader);

	GLint compile_status = 0;
	GL_CHECK(glGetShaderiv(shader->name, GL_COMPILE_STATUS, &compile_status));

	GLint log_length = 0;
	GL_CHECK(glGetShaderiv(shader->name, GL_INFO_LOG_LENGTH, &log_length));
	if (log_length > 0) {
		u8* buffer = arena->allocate(log_length);
		GLsizei length = 0;
		GL_CHECK(glGetShaderInfoLog(shader->name, log_length, &length, (GLchar*)buffer));
		*message = {buffer, (uword)length};
	} else {
		*message = nullptr;
	}
	return log_length;
}

}
