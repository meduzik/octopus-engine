#pragma once

#include "opengl.hpp"

namespace octo::render {
#if OCTO_RENDER_CHECKS
enum class RenderObjectTag : u64 {
	Context = 0x5192F363'A94F4ABBull,
	Device = 0xE15C0CCD'545EED24ull,
	Buffer = 0x75003084'9C90F7E8ull,
	Shader = 0x16AE3362'857F0687ull,
	Invalid = 0xF935E2C7'C7CC4942ull
};

#define OCTO_RENDER_OBJECT(t) \
	RenderObjectTag tag = RenderObjectTag::t; \
	static constexpr RenderObjectTag Tag = RenderObjectTag::t 
#else
#define OCTO_RENDER_OBJECT(t)
#endif

}
