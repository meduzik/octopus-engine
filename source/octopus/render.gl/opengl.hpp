#pragma once

// Print OpenGL driver details like extensions
#define OCTO_OPENGL_LOG_DETAILED_INFO 0

// Capture OpenGL debug messages from the GL_DEBUG_OUTPUT
#define OCTO_OPENGL_DEBUG_LOG 1
// Capture OpenGL debug messages from the GL_DEBUG_OUTPUT_SYNCHRONOUS
// (requires OCTO_OPENGL_DEBUG_LOG)
#define OCTO_OPENGL_DEBUG_LOG_SYNCHRONOUS 1

// Synchronously check every OpenGL call with glGetError
// Very slow
#define OCTO_OPENGL_SYNC_ERROR_CHECK 1

// Check for correct render api usage
#define OCTO_RENDER_CHECKS 1

#include <glad/glad.h>


