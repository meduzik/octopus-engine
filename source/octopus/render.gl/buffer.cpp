#include "buffer.hpp"
#include "impl.hpp"


namespace octo::render {

GLenum gl_usage_to_target[] = {
	GL_ARRAY_BUFFER,
	GL_ELEMENT_ARRAY_BUFFER
};

GLenum gl_access_to_usage[] = {
	GL_STATIC_DRAW,
	GL_DYNAMIC_DRAW,
	GL_STREAM_DRAW
};

#if OCTO_RENDER_CHECKS
bool IsValidBufferUsage(BufferUsage usage) {
	return usage == BufferUsage::IndexData || usage == BufferUsage::VertexData;
}

bool IsValidBufferAccess(BufferAccess access) {
	return access == BufferAccess::Dynamic || access == BufferAccess::Static || access == BufferAccess::Stream;
}
#endif

OCTO_PUBLIC Buffer* buffer_create(Device* device, BufferUsage usage, BufferAccess access, uword size, const u8* data) {
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(device);
	OCTO_RENDER_CHECK(IsValidBufferUsage(usage));
	OCTO_RENDER_CHECK(IsValidBufferAccess(access));
	OCTO_RENDER_CHECK(size <= MaxBufferSize);

	GLuint name = 0;
	GL_CHECK(glGenBuffers(1, &name));
	if (!name) {
		LOGw("Failed to allocate OpenGL buffer name");
		return nullptr;
	}
	Buffer* buffer = Mem.create<Buffer>();
	buffer->name = name;
	buffer->binding = (i32)usage;
#if OCTO_RENDER_CHECKS
	buffer->size = size;
	buffer->usage = usage;
#endif
	device_bind_buffer(device, buffer->binding, buffer->name);
	GL_CHECK(glBufferData(gl_buffer_bindings[buffer->binding], size, data, gl_access_to_usage[(i32)access]));
	return buffer;
}

OCTO_PUBLIC void buffer_upload(Device* device, Buffer* buffer, uword offset, uword size, const u8* data) {
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(device);
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(buffer);
	OCTO_RENDER_CHECK_NOT_NULL(data);
	OCTO_RENDER_CHECK(buffer->size >= offset);
	OCTO_RENDER_CHECK(buffer->size - offset >= size);
	device_bind_buffer(device, buffer->binding, buffer->name);
	GL_CHECK(glBufferSubData(gl_buffer_bindings[buffer->binding], offset, size, data));
}

OCTO_PUBLIC void buffer_destroy(Device* device, Buffer* buffer) {
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(device);
	OCTO_RENDER_CHECK_OBJECT_NOT_NULL(buffer);
	GL_CHECK(glDeleteBuffers(1, &buffer->name));
	OCTO_RENDER_DESTROY_OBJECT(buffer);
}


}
