#pragma once

#include <octopus/render/buffer.hpp>
#include "checks.hpp"
#include "impl.hpp"

namespace octo::render {

struct Buffer {
	OCTO_RENDER_OBJECT(Buffer);
	i32 binding;
	GLuint name;
#if OCTO_RENDER_CHECKS
	uword size;
	BufferUsage usage;
#endif
};

inline const GLenum gl_buffer_bindings[] = {
	GL_ARRAY_BUFFER,
	GL_ELEMENT_ARRAY_BUFFER
};

inline void device_bind_buffer(Device* device, i32 binding, GLuint name) {
	if (device->buffers[binding] != name) {
		GL_CHECK(glBindBuffer(gl_buffer_bindings[binding], name));
		device->buffers[binding] = name;
	}
}


}
