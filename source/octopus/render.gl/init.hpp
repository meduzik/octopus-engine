#pragma once

#include <octopus/render/device.hpp>

namespace octo::render {

struct Context;

Context* context_create();
void context_destroy(Context* context);

Device* device_create(Context* context);
void device_destroy(Device* device);

}
