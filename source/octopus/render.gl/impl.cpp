#include "impl.hpp"

namespace octo::render {

void gl_check(const char* location, const char* func, const char* call) {
	while (true) {
		GLenum err = glGetError();
		if (err == GL_NO_ERROR) {
			return;
		}
		log_report(LogLevel::Error, location, func, "OpenGL $err: $call", {
			{"err", err},
			{"call", call}
		});
	}
}

}
