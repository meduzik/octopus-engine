#define OCTO_HASH_TABLE_IMPL

#include <octopus/math/intrin.hpp>
#include <octopus/containers/impl/hashtable.hpp>

using namespace octo;

namespace octo::impl::hashtable {

const Ctrl EmptyGroup[] = {
	Ctrl_End,   Ctrl_Empty, Ctrl_Empty, Ctrl_Empty,
	Ctrl_Empty, Ctrl_Empty, Ctrl_Empty, Ctrl_Empty,
	Ctrl_Empty, Ctrl_Empty, Ctrl_Empty, Ctrl_Empty,
	Ctrl_Empty, Ctrl_Empty, Ctrl_Empty, Ctrl_Empty,
};

void RemoveSlot(Core* table, iword slotIndex) {
	Ctrl* ctrl = table->ctrl;

	iword leftIndex = (slotIndex - GroupSize) & (uword)table->capacity;

	u32 leftMask = Group(ctrl + leftIndex).MatchEmpty();
	u32 slotMask = Group(ctrl + slotIndex).MatchEmpty();

	bool reuse = leftMask && slotMask &&
		(octo_ctz32(leftMask) + octo_clz32(slotMask)) < GroupSize;

	SetCtrl(ctrl, table->capacity, slotIndex, reuse ? Ctrl_Empty : Ctrl_Tomb);
	table->free += reuse ? 1 : 0;
	--table->size;
}

InsertResult<void> FailCallback(
	Core* core, uword elemSize, uword hash, const void* context) {
	return { nullptr, false };
}

InsertResult<void> InsertWithCallback(
	Core* table, uword elemSize, uword hash,
	InsertCallback insertCallback, const void* insertContext) {
	iword slotIndex = FindFreeSlot(table, hash);

	Ctrl* ctrl = table->ctrl;
	iword free = table->free;
	Ctrl slotCtrl = ctrl[slotIndex];

	if (octo_unlikely(free == 0 && slotCtrl != Ctrl_Tomb))
		return insertCallback(table, elemSize, hash, insertContext);

	table->free = free - (slotCtrl == Ctrl_Empty ? 1 : 0);
	return InsertSlot(table, elemSize, hash, slotIndex);
}

InsertResult<void> InsertSlot(
	Core* table, uword elemSize, uword hash, iword slotIndex
) {
	++table->size;
	Ctrl* ctrl = table->ctrl;
	iword capacity = table->capacity;

	SetCtrl(ctrl, capacity, slotIndex, OCTO_HT_HASH2(hash));

	uword slotOffset = (uword)slotIndex * elemSize;
	return { OCTO_HT_SLOTS(ctrl, capacity) + slotOffset, true };
}

static void ConvertSpecialToEmptyAndFullToTomb(Ctrl* group) {
	__m128i ctrl = _mm_loadu_si128((const __m128i*)group);

	__m128i msb1 = _mm_set1_epi8(0b10000000u);
	__m128i lsb0 = _mm_set1_epi8(0b11111110u);

#if OCTO_SSSE3
	__m128i result = _mm_or_si128(_mm_shuffle_epi8(lsb0, ctrl), msb1);
#else
	__m128i zero = _mm_setzero_si128();
	__m128i mask = _mm_cmpgt_epi8(zero, ctrl);
	__m128i result = _mm_or_si128(msb1, _mm_andnot_si128(mask, lsb0));
#endif

	_mm_storeu_si128((__m128i*)group, result);
}

void ConvertTombToEmptyAndFullToTomb(Ctrl* ctrl, iword capacity) {
	octo_assert(ctrl[capacity] == Ctrl_End);

	Ctrl* end = ctrl + capacity + 1;
	for (Ctrl* group = ctrl; group != end; group += GroupSize)
		ConvertSpecialToEmptyAndFullToTomb(group);

	memcpy(end, ctrl, GroupSize);
	ctrl[capacity] = Ctrl_End;
}


}
