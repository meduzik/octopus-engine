#include <octopus/containers/twine.hpp>

namespace octo {

u8* Twine::write(u8* buf, const Twine* tw) {
	switch (tw->tag) {
	case twine_tag::empty: {
		return 0;
	} break;
	case twine_tag::string_ref: {
		return write(buf, tw->value.sr);
	} break;
	case twine_tag::concat_tw_tw: {
		return write(write(buf, tw->value.concat_tt.lhs), tw->value.concat_tt.rhs);
	} break;
	case twine_tag::concat_sr_tw: {
		return write(write(buf, tw->value.concat_srt.lhs), tw->value.concat_srt.rhs);
	} break;
	case twine_tag::concat_tw_sr: {
		return write(write(buf, tw->value.concat_tsr.lhs), tw->value.concat_tsr.rhs);
	} break;
	case twine_tag::concat_sr_sr: {
		return write(write(buf, tw->value.concat_srsr.lhs), tw->value.concat_srsr.rhs);
	} break;
	default: octo_unreachable;
	}
	
}

u8* Twine::write(u8* buf, StringRef sr) {
	uword size = sr.size();
	memcpy(buf, sr.data(), size);
	return buf + size;
}

}
