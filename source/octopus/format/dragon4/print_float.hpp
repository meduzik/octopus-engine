#pragma once

/******************************************************************************
  Modified source of the Dragon4 algorithm.

  Original source by Ryan Juckett
  http://www.ryanjuckett.com/
******************************************************************************/

namespace octo::dragon4 {
//******************************************************************************
//******************************************************************************
enum tPrintFloatFormat
{
    PrintFloatFormat_Positional,    // [-]ddddd.dddd
    PrintFloatFormat_Scientific,    // [-]d.dddde[sign]ddd
};
 
//******************************************************************************
// Print a 32-bit floating-point number as a decimal string.
// The output string is always NUL terminated and the string length (not
// including the NUL) is returned.
//******************************************************************************
octo_noinline
u32 PrintFloat32
(
    char *               pOutBuffer,     // buffer to output into
    u32                bufferSize,     // size of pOutBuffer
    f32                value,          // value to print
    tPrintFloatFormat   format,         // format to print with
    i32                precision       // If negative, the minimum number of digits to represent a
                                        // unique 32-bit floating point value is output. Otherwise,
                                        // this is the number of digits to print past the decimal point.
);
 
//******************************************************************************
// Print a 64-bit floating-point number as a decimal string.
// The output string is always NUL terminated and the string length (not
// including the NUL) is returned.
//******************************************************************************
octo_noinline
u32 PrintFloat64
(
    char *               pOutBuffer,     // buffer to output into
    u32                bufferSize,     // size of pOutBuffer
    f64                value,          // value to print
    tPrintFloatFormat   format,         // format to print with
    i32                precision       // If negative, the minimum number of digits to represent a
                                        // unique 64-bit floating point value is output. Otherwise,
                                        // this is the number of digits to print past the decimal point.
);

}
