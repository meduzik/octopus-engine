#pragma once

/******************************************************************************
  Modified source of the Dragon4 algorithm.

  Original source by Ryan Juckett
  http://www.ryanjuckett.com/
******************************************************************************/

#include <octopus/core/types.hpp>
#include <octopus/macros/api.h>

namespace octo::dragon4 {

//******************************************************************************
// Different modes for terminating digit output
//******************************************************************************
enum tCutoffMode
{
    CutoffMode_Unique,          // as many digits as necessary to print a uniquely identifiable number
    CutoffMode_TotalLength,     // up to cutoffNumber significant digits
    CutoffMode_FractionLength,  // up to cutoffNumber significant digits past the decimal point
};

//******************************************************************************
// This is an implementation the Dragon4 algorithm to convert a binary number
// in floating point format to a decimal number in string format. The function
// returns the number of digits written to the output buffer and the output is
// not NUL terminated.
//
// The floating point input value is (mantissa * 2^exponent).
//
// See the following papers for more information on the algorithm:
//  "How to Print Floating-Point Numbers Accurately"
//    Steele and White
//    http://kurtstephens.com/files/p372-steele.pdf
//  "Printing Floating-Point Numbers Quickly and Accurately"
//    Burger and Dybvig
//    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.72.4656&rep=rep1&type=pdf
//******************************************************************************
u32 Dragon4
(
    u64        mantissa,           // value significand
    i32        exponent,           // value exponent in base 2
    u32        mantissaHighBitIdx, // index of the highest set mantissa bit
    bool          hasUnequalMargins,  // is the high margin twice as large as the low margin
    tCutoffMode cutoffMode,         // how to determine output length
    u32        cutoffNumber,       // parameter to the selected cutoffMode
    char *       pOutBuffer,         // buffer to output into
    u32        bufferSize,         // maximum characters that can be printed to pOutBuffer
    i32 *      pOutExponent        // the base 10 exponent of the first digit
);

}
