#include <octopus/utils/time.hpp>
#include <chrono>

namespace octo {

static u64 timestamp_appstart_value = timestamp_now();

OCTO_PUBLIC
u64 timestamp_appstart() {
	return timestamp_appstart_value;
}

OCTO_PUBLIC
u64 timestamp_now() {
	const auto now = std::chrono::system_clock::now();
	return std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch()).count();
}


}