#include <octopus/utils/cityhash.hpp>

namespace octo {

u64 CityHash::CityHash64(const void* data, uword size) {
	//TODO: implement CityHash64

	uword hash = 0xcbf29ce484222325;
	for (uword i = 0; i < size; ++i) {
		hash ^= ((const u8*)data)[i];
		hash *= 0x100000001b3;
	}
	return hash;
}

}
