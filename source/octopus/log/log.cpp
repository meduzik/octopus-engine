#include <octopus/log/log.hpp>
#include <octopus/utils/time.hpp>
#include <octopus/threading/thread.hpp>
#include <octopus/format/format.hpp>

namespace octo {

LogWriter::LogWriter(Arena& arena) :
	arena(&arena),
	skiprefs(arena.create<ArenaVector<u32>>(&arena)),
	ptr(nullptr),
	base(nullptr),
	limit(nullptr)
{
	select_message_container((LogMessage*)&inline_message, LogMessage::DataInlineSize);
}

void LogWriter::select_message_container(LogMessage* message, uword data_size) {
	active_message = message;
	ptr = active_message->data + (ptr - base);
	base = active_message->data;
	limit = base + data_size;
}

LogWriter::~LogWriter() {
}

u8* LogWriter::slow_write_grow(uword count) {
	uword old_cap = (limit - base);
	uword new_cap = std::max(old_cap + count, old_cap * 4);
	LogMessage* new_message = (LogMessage*)arena->allocate(sizeof(LogMessage) + new_cap);
	memcpy(new_message, active_message, sizeof(LogMessage) + ptr - base);
	select_message_container(new_message, new_cap);
	return write(count);
}


static std::mutex mutex_log;
static MemVector<ILogSink*> log_sinks;


static void log_dispatch(LogMessage* message) {
	std::lock_guard<std::mutex> lock(mutex_log);

	for (auto sink: log_sinks) {
		sink->accept_message(message);
	}
}

OCTO_PUBLIC
void log_add_sink(ILogSink* sink) {
	log_sinks.push_back(sink);
}

OCTO_PUBLIC
void log_remove_sink(ILogSink* sink) {
	auto n = log_sinks.size();
	for (uword i = 0; i < n; i++) {
		if (log_sinks[i] == sink) {
			log_sinks[i] = log_sinks.back();
			log_sinks.pop_back();
			return;
		}
	}
	octo_assert(false, "removing non-existent log sink");
}


octo_noinline
void OCTO_PUBLIC log_report(LogLevel level, const char* location, const char* function, const char* text, std::initializer_list<LogArg> args) {
	Arena arena;
	LogWriter writer(arena);
	LogMessage* message = writer.get_message();
	message->level = level;
	message->location = location;
	message->function = function;
	message->message = text;	
	message->timestamp = timestamp_now();
	message->thread_id = thread_get_id();
	for (auto& arg: args) {
		writer.object_begin(arg.name);
		arg.value.vt->write_object_fn(arg.value.object, writer);
		writer.object_end();
	}
	writer.value(nullptr);
#if OCTO_LOG_MODE == OCTO_LOG_MODE_IMMEDIATE
	log_dispatch(writer.get_message());
#else
	#error Delayed log mode not implemented
#endif
}

struct OutputBuffer {
	OutputBuffer(Arena* arena) :
		storage(arena) {
	}

	void put(u8 ch) {
		storage.push_back(ch);
	}

	template<size_t N>
	void write(const char(&c_str)[N]) {
		write((const u8*)c_str, N);
	}

	void write(StringRef s) {
		write(s.data(), s.size());
	}

	void write(const u8* data, uword len) {
		uword size = storage.size();
		storage.resize(size + len);
		memcpy(storage.data() + size, data, len);
	}

	ArenaVector<u8> storage;
};

static const u8 HexDigit[] = "0123456789abcdef";

static const u8* log_format_id(OutputBuffer& buffer, const u8* data) {
	octo_assert(*data == (u8)LogWriter::ValueTag::String);
	data++;
	u32 id_len;
	memcpy(&id_len, data, sizeof(id_len));
	data += sizeof(id_len);
	buffer.write(data, id_len);
	data += id_len;
	return data;
}

static const u8* log_format_data(OutputBuffer& buffer, const u8* data) {
	switch ((LogWriter::ValueTag)*data) {
	case LogWriter::ValueTag::Null: {
		buffer.write("null");
		return data + 1;
	} break;
	case LogWriter::ValueTag::BoolFalse: {
		buffer.write("false");
		return data + 1;
	} break;
	case LogWriter::ValueTag::BoolTrue: {
		buffer.write("true");
		return data + 1;
	} break;
	case LogWriter::ValueTag::Int: {
		i64 val;
		memcpy(&val, data + 1, sizeof(val));
		buffer.write(format::decimal(val));
		return data + 1 + sizeof(val);
	} break;
	case LogWriter::ValueTag::UInt: {
		u64 val;
		memcpy(&val, data + 1, sizeof(val));
		buffer.write(format::decimal(val));
		return data + 1 + sizeof(val);
	} break;
	case LogWriter::ValueTag::Float: {
		f32 val;
		memcpy(&val, data + 1, sizeof(val));
		buffer.write(format::shortest(val));
		return data + 1 + sizeof(val);
	} break;
	case LogWriter::ValueTag::Double: {
		f64 val;
		memcpy(&val, data + 1, sizeof(val));
		buffer.write(format::shortest(val));
		return data + 1 + sizeof(val);
	} break;
	case LogWriter::ValueTag::ByteString: {
		data++;
		buffer.write("#");
		u32 str_len;
		memcpy(&str_len, data, sizeof(str_len));
		data += sizeof(str_len);
		for (uword i = 0; i < str_len; i++) {
			if (i != 0) {
				buffer.put(' ');
			}
			buffer.put(HexDigit[data[i] >> 4]);
			buffer.put(HexDigit[data[i] % 16]);			
		}
		buffer.put('#');
		return data + str_len;
	} break;
	case LogWriter::ValueTag::String: {
		data++;
		buffer.put('"');
		u32 str_len;
		memcpy(&str_len, data, sizeof(str_len));
		data += sizeof(str_len);
		buffer.write(data, str_len);
		buffer.put('"');
		return data + str_len;
	} break;
	case LogWriter::ValueTag::Array: {
		data += 5;
		buffer.put('[');
		bool is_first = true;
		while (*data != (u8)LogWriter::ValueTag::EndMarker) {
			if (!is_first) {
				buffer.put(',');
			}
			is_first = false;
			data = log_format_data(buffer, data);
		}
		buffer.put(']');
		data++;
		return data;
	} break;
	case LogWriter::ValueTag::Dictionary: {
		data += 5;
		buffer.put('{');
		bool is_first = true;
		while (*data != (u8)LogWriter::ValueTag::EndMarker) {
			if (!is_first) {
				buffer.put(',');
			}
			is_first = false;
			data = log_format_data(buffer, data);
			buffer.put(':');
			data = log_format_data(buffer, data);
		}
		buffer.put('}');
		data++;
		return data;
	} break;
	case LogWriter::ValueTag::Object: {
		data += 5;
		data = log_format_id(buffer, data);
		buffer.put('(');
		bool is_first = true;
		while (*data != (u8)LogWriter::ValueTag::EndMarker) {
			if (!is_first) {
				buffer.put(',');
			}
			is_first = false;
			data = log_format_id(buffer, data);
			buffer.put('=');
			data = log_format_data(buffer, data);
		}
		buffer.put(')');
		data++;
		return data;
	} break;
	default: {
		octo_unreachable;
	} break;
	}
}

OCTO_PUBLIC
StringRef log_format_string(Arena* arena, StringRef message, const u8* data) {
	ArenaVector<std::pair<StringRef, const u8*>> args(arena);

	// preparse arguemnts
	while (*data != (u8)LogWriter::ValueTag::Null) {
		data++;
		u32 skip_len;
		memcpy(&skip_len, data, sizeof(skip_len));
		data += sizeof(skip_len);
		const u8* data_next = data + skip_len;
		u32 key_len;
		const u8* key_data;
		data++; // skip key string tag
		memcpy(&key_len, data, sizeof(key_len));
		key_data = data + sizeof(key_len);
		data += sizeof(key_len) + key_len;
		args.push_back(StringRef{key_data, key_len}, data);
		data = data_next;
	}

	OutputBuffer buffer(arena);

	// parse format string & attach arguments
	const u8* input = message.data();
	const u8* end = message.end();
	const u8* word_begin = input;
	while (input != end) {
		if (*input == '$') {
			if (word_begin != input) {
				buffer.write(word_begin, input - word_begin);
			}
			input++;
			if (input == end || *input == '$') {
				buffer.put('$');
			} else {
				word_begin = input;
				while (input != end) {
					if (!isalnum(*input)) {
						break;
					}
					input++;
				}
				if (word_begin == input) {
					buffer.put('$');
				} else {
					StringRef argname(word_begin, input);
					for (auto& arg: args) {
						if (arg.first == argname) {
							log_format_data(buffer, arg.second);
						}
					}
				}
				word_begin = input;
			}
		} else {
			input++;
		}
	}
	if (word_begin != input) {
		buffer.write(word_begin, input - word_begin);
	}
	return StringRef(buffer.storage.data(), buffer.storage.size());
}

class StderrLogSink: public ILogSink {
public:
	virtual void accept_message(LogMessage* message) override {
		Arena arena;
		u64 time = message->timestamp - timestamp_appstart();
		u32 ms = (u32)((time + 500) / 1000) % 1000;
		u32 s = (u32)(time / 1'000'000);
		char level_id;
		switch (message->level) {
		case LogLevel::Debug: {
			level_id = 'd';
		} break;
		case LogLevel::Error: {
			level_id = 'E';
		} break;
		case LogLevel::Fatal: {
			level_id = 'F';
		} break;
		case LogLevel::Info: {
			level_id = 'i';
		} break;
		case LogLevel::Verbose: {
			level_id = 'v';
		} break;
		case LogLevel::Warning: {
			level_id = 'w';
		} break;
		default: {
			octo_unreachable;
		} break;
		}
		StringRef formatted = log_format_string(&arena, message->message, message->data);
		fprintf(
			stderr,
			//"[%d.%03d] %c/%s(%s)@%x: %.*s\n",
			"[%d.%03d] %c/@%x: %.*s\n",
			s,
			ms,
			level_id,
			//message->location,
			//message->function,
			message->thread_id,
			(int)formatted.size(),
			(const char*)formatted.data()
		);
	}
};

OCTO_PUBLIC
ILogSink* log_get_stderr_sink() {
	static StderrLogSink sink;
	return &sink;
}


void LogWriter::value(nullptr_t) {
	u8* out = write(1);
	*out = (u8)ValueTag::Null;
}
void LogWriter::value_bool(bool val) {
	u8* out = write(1);
	*out = (u8)(val ? ValueTag::BoolTrue : ValueTag::BoolFalse);
}
void LogWriter::value(u8 val) {
	value((i64)val);
}
void LogWriter::value(u16 val) {
	value((i64)val);
}
void LogWriter::value(u32 val) {
	value((i64)val);
}
void LogWriter::value(i8 val) {
	value((i64)val);
}
void LogWriter::value(i16 val) {
	value((i64)val);
}
void LogWriter::value(i32 val) {
	value((i64)val);
}
void LogWriter::value(u64 val) {
	u8* out = write(1 + sizeof(val));
	*out = (u8)ValueTag::UInt;
	memcpy(out + 1, &val, sizeof(val));
}
void LogWriter::value(i64 val) {
	u8* out = write(1 + sizeof(val));
	*out = (u8)ValueTag::Int;
	memcpy(out + 1, &val, sizeof(val));
}
void LogWriter::value(f32 val) {
	u8* out = write(1 + sizeof(val));
	*out = (u8)ValueTag::Float;
	memcpy(out + 1, &val, sizeof(val));
}
void LogWriter::value(f64 val) {
	u8* out = write(1 + sizeof(val));
	*out = (u8)ValueTag::Double;
	memcpy(out + 1, &val, sizeof(val));
}
void LogWriter::key(StringRef val) {
	value(val);
}
void LogWriter::value(StringRef val) {
	string_begin();
	string_contents(val.data(), val.size());
	string_end();
}
void LogWriter::key(const Twine& val) {
	value(val);
}
void LogWriter::value(const Twine& val) {
	string_begin();
	val.visit([&](StringRef s) {
		string_contents(s);
	});
	string_end();
}
void LogWriter::array_begin() {
	u8* out = write(5);
	*out = (u8)ValueTag::Array;
	store_skip_ref(out + 1);
}
void LogWriter::array_end() {
	u8* out = write(1);
	*out = (u8)ValueTag::EndMarker;
	resolve_skip_ref();
}
void LogWriter::string_begin() {
	u8* out = write(5);
	*out = (u8)ValueTag::String;
	store_skip_ref(out + 1);
}
void LogWriter::string_end() {
	resolve_skip_ref();
}
void LogWriter::bytestring_begin() {
	u8* out = write(5);
	*out = (u8)ValueTag::String;
	store_skip_ref(out + 1);
}
void LogWriter::bytestring_end() {
	resolve_skip_ref();
}
void LogWriter::string_contents(const u8* data, uword len) {
	u8* out = write(len);
	memcpy(out, data, len);
}
void LogWriter::string_contents(StringRef val) {
	string_contents(val.data(), val.size());
}
void LogWriter::bytestring_contents(const u8* data, uword len) {
	u8* out = write(len);
	memcpy(out, data, len);
}
void LogWriter::dictionary_begin() {
	u8* out = write(5);
	*out = (u8)ValueTag::Dictionary;
	store_skip_ref(out + 1);
}
void LogWriter::dictionary_end() {
	u8* out = write(1);
	*out = (u8)ValueTag::EndMarker;
	resolve_skip_ref();
}
void LogWriter::object_begin() {
	u8* out = write(5);
	*out = (u8)ValueTag::Object;
	store_skip_ref(out + 1);
}
void LogWriter::object_begin(StringRef name) {
	object_begin();
	value(name);
}
void LogWriter::object_end() {
	u8* out = write(1);
	*out = (u8)ValueTag::EndMarker;
	resolve_skip_ref();
}

u8* LogWriter::write(uword count) {
	if (octo_unlikely((uword)(limit - ptr) < count)) {
		return slow_write_grow(count);
	}
	u8* ret = ptr;
	ptr += count;
	return ret;
}

void LogWriter::store_skip_ref(u8* ptr) {
	skiprefs->push_back((u32)(ptr - base));
}

void LogWriter::resolve_skip_ref() {
	u32 refpos = skiprefs->back();
	skiprefs->pop_back();
	u32 refval = (u32)(ptr - base - refpos - 4);
	memcpy(base + refpos, &refval, 4);
}


}
