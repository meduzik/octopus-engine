#include <octopus/log/log.hpp>
#include <octopus/platform.win/windows.hpp>
#include <octopus/platform.win/string.hpp>
#include <octopus/utils/time.hpp>
#include <octopus/format/format.hpp>
#include <fcntl.h>
#include <io.h>

namespace octo {

class WinConsoleLogSink: public ILogSink {
public:
	WinConsoleLogSink() {
		AllocConsole();
		output = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(output, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);
		redirect_stdio_to_console();
	}

	virtual void accept_message(LogMessage* message) override {
		Arena arena;

		char level_char;		
		WORD attrs;
		switch (message->level) {
		case LogLevel::Debug: {
			attrs = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY;
			level_char = 'd';
		} break;
		case LogLevel::Verbose: {
			attrs = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN;
			level_char = 'v';
		} break;
		case LogLevel::Info: {
			attrs = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
			level_char = 'i';
		} break;
		case LogLevel::Warning: {
			attrs = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
			level_char = 'w';
		} break;
		case LogLevel::Error: {
			attrs = FOREGROUND_RED | FOREGROUND_INTENSITY;
			level_char = 'E';
		} break;
		case LogLevel::Fatal: {
			attrs = BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
			level_char = 'F';
		} break;
		default: {
			octo_unreachable;
		} break;
		}

		u64 elapsed_time = message->timestamp - timestamp_appstart();
		u32 seconds = (u32)(elapsed_time / 1'000'000);
		u32 milliseconds = (u32)(elapsed_time / 1'000 % 1'000);

		char prefix[64];
		sprintf(prefix, "[%d.%03d] %c/%x: ", seconds, milliseconds, level_char, message->thread_id);
		StringRef formatted_string = log_format_string(&arena, message->message, message->data);

		auto formatted_message = win::widen(&arena, (
			StringRef{(const u8*)prefix, strlen(prefix)}
			+
			formatted_string
			+
			"\n"
		).allocate(arena));

		SetConsoleTextAttribute(output, attrs);

		DWORD written_num = 0;
		WriteConsoleW(output, formatted_message.data(), (DWORD)formatted_message.size(), &written_num, nullptr);
	}

	void redirect_stdio_to_console() {
		int hConHandle;
		HANDLE lStdHandle;
		CONSOLE_SCREEN_BUFFER_INFO coninfo;
		FILE* fp;

		// allocate a console for this app
		AllocConsole();

		// set the screen buffer to be big enough to let us scroll text
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
		coninfo.dwSize.Y = 500;
		SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

		// redirect unbuffered STDOUT to the console
		lStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		hConHandle = _open_osfhandle((intptr_t)lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "w");
		*stdout = *fp;
		setvbuf(stdout, NULL, _IONBF, 0);
		freopen_s(&fp, "CONOUT$", "w", stdout);

		// redirect unbuffered STDIN to the console
		lStdHandle = GetStdHandle(STD_INPUT_HANDLE);
		hConHandle = _open_osfhandle((intptr_t)lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "r");
		*stdin = *fp;
		setvbuf(stdin, NULL, _IONBF, 0);
		freopen_s(&fp, "CONIN$", "r", stdin);

		// redirect unbuffered STDERR to the console
		lStdHandle = GetStdHandle(STD_ERROR_HANDLE);
		hConHandle = _open_osfhandle((intptr_t)lStdHandle, _O_TEXT);
		fp = _fdopen(hConHandle, "w");
		*stderr = *fp;
		setvbuf(stderr, NULL, _IONBF, 0);
		freopen_s(&fp, "CONOUT$", "w", stderr);
	}

	HANDLE output;
};

OCTO_PUBLIC
ILogSink* log_create_win_console_sink() {
	static WinConsoleLogSink sink;

	return &sink;
}

class WinOutputLogSink: public ILogSink {
public:
	virtual void accept_message(LogMessage* message) override {
		Arena arena;

		char level_char;
		WORD attrs;
		switch (message->level) {
		case LogLevel::Debug: {
			attrs = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY;
			level_char = 'd';
		} break;
		case LogLevel::Verbose: {
			attrs = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN;
			level_char = 'v';
		} break;
		case LogLevel::Info: {
			attrs = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
			level_char = 'i';
		} break;
		case LogLevel::Warning: {
			attrs = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
			level_char = 'w';
		} break;
		case LogLevel::Error: {
			attrs = FOREGROUND_RED | FOREGROUND_INTENSITY;
			level_char = 'E';
		} break;
		case LogLevel::Fatal: {
			attrs = BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
			level_char = 'F';
		} break;
		default: {
			octo_unreachable;
		} break;
		}

		u64 elapsed_time = message->timestamp - timestamp_appstart();
		u32 seconds = (u32)(elapsed_time / 1'000'000);
		u32 milliseconds = (u32)(elapsed_time / 1'000 % 1'000);

		char prefix[64];
		sprintf(prefix, "[%d.%03d] %c/%x: ", seconds, milliseconds, level_char, message->thread_id);
		StringRef formatted_string = log_format_string(&arena, message->message, message->data);

		auto formatted_message = win::widen(&arena, (
			StringRef{ (const u8*)prefix, strlen(prefix) }
			+
			message->location
			+
			": "
			+
			formatted_string
			+
			"\n"
		).allocate(arena));

		OutputDebugStringW((LPCWSTR)formatted_message.data());
	}
};

OCTO_PUBLIC
ILogSink* log_create_win_debug_output_sink() {
	static WinOutputLogSink sink;

	return &sink;
}


}
