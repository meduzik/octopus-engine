#include <octopus/platform.win/utils.hpp>

namespace octo::win {

OCTO_PUBLIC bool is_debugger() {
	return IsDebuggerPresent();
}


}
