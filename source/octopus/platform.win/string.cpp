#include <octopus/platform.win/string.hpp>

namespace octo::win {

OCTO_PUBLIC Span<u16> widen(Arena* arena, StringRef string) {
	int size = MultiByteToWideChar(
		CP_UTF8,
		0,
		(LPCCH)string.data(),
		(int)string.size(),
		nullptr,
		0
	);
	u16* buffer = (u16*)arena->allocate((size + 1) * sizeof(u16));
	int written = MultiByteToWideChar(
		CP_UTF8,
		0,
		(LPCCH)string.data(),
		(int)string.size(),
		(LPWSTR)buffer,
		size
	);
	buffer[written] = 0;
	return { buffer, (uword)written };
}

OCTO_PUBLIC StringRef narrow(Arena* arena, Span<u16> string) {
	int size = WideCharToMultiByte(
		CP_UTF8,
		0,
		(LPCWCH)string.data(),
		(int)string.size(),
		nullptr,
		0,
		nullptr,
		nullptr
	);
	u8* buffer = arena->allocate(size);
	int written = WideCharToMultiByte(
		CP_UTF8,
		0,
		(LPCWCH)string.data(),
		(int)string.size(),
		(LPSTR)buffer,
		size,
		nullptr,
		nullptr
	);
	buffer[written] = 0;
	return {buffer, (uword)written};
}

}
