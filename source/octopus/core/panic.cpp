#include <octopus/core/panic.h>
#include <octopus/log/log.hpp>

using namespace octo;

void octo_default_panic_handler(void*, const char* file, int line, const char* function, const char* message){
	LOGf("PANIC! \n  File $file, line $line,\n  Function ($func):\n    $message", {"file", file}, {"line", line}, {"func", function},  {"message", message});
	abort();
}

static octo_panic_handler octo_panic_handler_instance{nullptr, octo_default_panic_handler};

OCTO_C octo_panic_handler OCTO_PUBLIC octo_panic_handler_set(octo_panic_handler new_handler){
	octo_panic_handler old_handler = octo_panic_handler_instance;
	octo_panic_handler_instance = new_handler;
	return octo_panic_handler_instance;
}

OCTO_C octo_panic_handler OCTO_PUBLIC octo_panic_handler_get(){
	return octo_panic_handler_instance;
}

OCTO_C OCTO_NORETURN
void OCTO_PUBLIC octo_raise_panic(const char* file, int line, const char* function, const char* message){
	octo_panic_handler_instance.func(octo_panic_handler_instance.ud, file, line, function, message);
	abort();
}
