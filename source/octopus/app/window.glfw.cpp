#include <octopus/app/window.hpp>
#include <octopus/macros/utils.h>
#include <octopus/log/log.hpp>
#include "window.glfw.hpp"

namespace octo {


static void glfw_window_on_resized(Window* window, i32 width, i32 height) {
	LOGv("Window $win resized to $w x $h", { "win", (uword)window->id }, { "w", width }, { "h", height });
	glfw_surface_on_window_resized(&window->surface, width, height);
}

static void glfw_framebuffer_on_resized(Window* window, i32 width, i32 height) {
	LOGv("Framebuffer for $win resized to $w x $h", { "win", (uword)window->id }, { "w", width }, { "h", height });
	glfw_surface_on_framebuffer_resized(&window->surface, width, height);
}

static void glfw_window_size_callback(GLFWwindow* glfw_window, int width, int height) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	glfw_window_on_resized(window, width, height);
}

static void glfw_window_framebuffer_size_callback(GLFWwindow* glfw_window, int width, int height) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	glfw_framebuffer_on_resized(window, width, height);
}

static void glfw_window_key_callback(GLFWwindow* glfw_window, int key, int scancode, int action, int mods) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	Surface* surface = &window->surface;
	surface->listener->on_key_press(surface, scancode, (action == GLFW_RELEASE ? ButtonState::Up : ButtonState::Down));
}

static void glfw_window_cursor_position_callback(GLFWwindow* glfw_window, double xpos, double ypos) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	Surface* surface = &window->surface;
	surface->listener->on_mouse_move(surface, xpos, ypos);
}

static void glfw_window_cursor_enter_callback(GLFWwindow* glfw_window, int entered) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	Surface* surface = &window->surface;
	if (entered == GLFW_TRUE) {
		surface->listener->on_mouse_enter(surface);
	} else {
		surface->listener->on_mouse_leave(surface);
	}
}

static void glfw_window_mouse_button_callback(GLFWwindow* glfw_window, int button, int action, int mods) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	Surface* surface = &window->surface;
	MouseButton mouse_button;
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT: {
		mouse_button = MouseButton::Left;
	} break;
	case GLFW_MOUSE_BUTTON_RIGHT: {
		mouse_button = MouseButton::Right;
	} break;
	case GLFW_MOUSE_BUTTON_MIDDLE: {
		mouse_button = MouseButton::Middle;
	} break;
	default: {
		mouse_button = (MouseButton)((u8)MouseButton::Extra0 + (button - GLFW_MOUSE_BUTTON_4));
	} break;
	}
	surface->listener->on_mouse_press(surface, mouse_button, (action == GLFW_PRESS ? ButtonState::Down : ButtonState::Up));
}

static void glfw_window_scroll_callback(GLFWwindow* glfw_window, double xoffset, double yoffset) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	Surface* surface = &window->surface;
	surface->listener->on_scroll(surface, xoffset, yoffset);
}

static void glfw_window_close_callback(GLFWwindow* glfw_window) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	LOGv("User wants to close window $win", { "win", window->id });
	if (!window->listener->on_close(window)) {
		LOGv("Callback rejects closure for window $win", { "win", window->id });
		glfwSetWindowShouldClose(glfw_window, GLFW_FALSE);
	} else {
		LOGv("Callback approves closure for window $win", { "win", window->id });
	}
}

static void glfw_window_focus_callback(GLFWwindow* glfw_window, int focus) {
	Window* window = (Window*)glfwGetWindowUserPointer(glfw_window);
	if (focus) {
		LOGv("Window $win gains focus", {"win", window->id });
	} else {
		LOGv("Window $win loses focus", { "win", window->id });
	}	
	window->listener->on_focus(window, focus);
}

#if OCTO_OPENGL_LOG_DETAILED_INFO
struct OpenGLExtensionToken {
};

void write_object(LogWriter& writer, OpenGLExtensionToken) {
	GLint num_extensions = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &num_extensions);
	writer.array_begin();
	for (GLint i = 0; i < num_extensions; i++) {
		writer.value((const char*)glGetStringi(GL_EXTENSIONS, i));
	}
	writer.array_end();
}
#endif

OCTO_PUBLIC
Window* window_create(Application* app, const WindowCreationParams* params, IWindowListener* listener) {
	octo_require(listener != nullptr);

	Arena arena;
	Window* window = &app->main_window;
	if (window->created) {
		octo_panic("Multiple windows not supported");
	}
	
	LOGv("Creating GLFW application window...");

	glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);

#if !OCTO_OPENGL_SYNC_ERROR_CHECK
	glfwWindowHint(GLFW_CONTEXT_NO_ERROR, 1);
#endif

	const char* title = params->title.copy_null_terminated(arena);
	window->window = glfwCreateWindow(params->width, params->height, title, nullptr, nullptr);
	if (!window->window) {
		octo_panic("Failed to create GLFW window");
	}
	window->created = true;
	glfwSetWindowUserPointer(window->window, window);

	LOGv("GLFW application window created ($win)", { "win", window->id });

	LOGv("Configuring OpenGL context fopr GLFW application window $win...", { "win", window->id });

	glfwMakeContextCurrent(window->window);
	glfwSwapInterval(1);

	LOGv("Executing GLAD loader...");
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		LOGv("GLAD loader succeeded");
	} else {
		octo_panic("GLAD loader failed");
	}
#if OCTO_OPENGL_LOG_DETAILED_INFO
	LOGi(
		"OpenGL info:\n  Version: $ver\n  Vendor: $vendor\n  Renderer: $renderer\n  Extensions: $extensions",
		{ "ver", (const char*)glGetString(GL_VERSION) },
		{ "vendor", (const char*)glGetString(GL_VENDOR) },
		{ "renderer", (const char*)glGetString(GL_RENDERER) },
		{ "extensions", OpenGLExtensionToken{} }
	);
#else
	LOGi("OpenGL version $ver", { "ver", (const char*)glGetString(GL_VERSION) });
#endif
	LOGv("GLFW application window ($win) OpenGL context configured", { "win", window->id });

	LOGv("GLFW creating surface for window ($win)...", { "win", window->id });

	glfw_surface_init(window, &window->surface);

	LOGv("GLFW installing callbacks for window ($win)...", { "win", window->id });

	glfwSetWindowCloseCallback(window->window, glfw_window_close_callback);
	glfwSetWindowSizeCallback(window->window, glfw_window_size_callback);
	glfwSetKeyCallback(window->window, glfw_window_key_callback);
	glfwSetCursorPosCallback(window->window, glfw_window_cursor_position_callback);
	glfwSetCursorEnterCallback(window->window, glfw_window_cursor_enter_callback);
	glfwSetMouseButtonCallback(window->window, glfw_window_mouse_button_callback);
	glfwSetScrollCallback(window->window, glfw_window_scroll_callback);
	glfwSetWindowFocusCallback(window->window, glfw_window_focus_callback);
	glfwSetFramebufferSizeCallback(window->window, glfw_window_framebuffer_size_callback);

	window->listener = listener;
	window->listener->on_bind(window);

	LOGv("GLFW capturing buffer size of window ($win)...", { "win", window->id });

	int width, height;

	glfwGetWindowSize(window->window, &width, &height);
	glfw_window_on_resized(window, width, height);

	glfwGetFramebufferSize(window->window, &width, &height);
	glfw_framebuffer_on_resized(window, width, height);

	LOGv("GLFW application window ($win) configured", { "win", window->id });
	
	return window;
}

OCTO_PUBLIC
Surface* window_get_surface(Window* win) {
	return &win->surface;
}

OCTO_PUBLIC
void window_set_mouse_capture(Window* window, bool capture) {
	if (capture) {
		glfwSetInputMode(window->window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	} else {
		glfwSetInputMode(window->window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

OCTO_PUBLIC
void window_destroy(Window* win) {
	glfw_surface_destroy(win, &win->surface);
	if (win->listener) {
		win->listener->on_unbind(win);
		win->listener = nullptr;
	}
	if (win->window) {
		glfwMakeContextCurrent(nullptr);
		glfwDestroyWindow(win->window);
		win->window = nullptr;
	}
}


}
