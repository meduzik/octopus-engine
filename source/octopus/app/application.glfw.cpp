#include <octopus/app/application.hpp>
#include <octopus/log/log.hpp>
#include "glfw.hpp"
#include "window.glfw.hpp"

namespace octo {

void application_glfw_error_callback(int error_code, const char* description) {
	LOGe("GLFW error $code: $message", { "code", error_code }, { "message", description });
}

static std::mutex glfw_init_mutex;

static void application_glfw_init() {
	std::scoped_lock<std::mutex> lock(glfw_init_mutex);

	LOGv("Initializing GLFW...");

	glfwSetErrorCallback(application_glfw_error_callback);

	if (glfwInit() == GLFW_FALSE) {
		octo_panic("Failed to initialize GLFW");
	} else {
		LOGv("GLFW Initialized!");
	}
}

static void application_glfw_create(Application* app) {
	application_glfw_init();
}

OCTO_PUBLIC
Application* application_create() {
	LOGv("Creating GLFW application...");

	Application* app = Mem.create<Application>();
	application_glfw_create(app);

	LOGv("GLFW application created");

	return app;
}

OCTO_PUBLIC
void application_loop(Application* app) {
	Window* window = &app->main_window;
	while (!glfwWindowShouldClose(window->window)) {
		glfwPollEvents();
		if (glfw_surface_validate(&window->surface)) {
			glfw_surface_update(&window->surface);

			if (glfw_surface_begin_render(&window->surface)) {
				glfw_surface_render(&window->surface);
				glfw_surface_end_render(&window->surface);
				glfwSwapBuffers(window->window);
			}
		} else {
			glfw_surface_skip_update(&window->surface);

			// reduce fps to 10 in the minimized state
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}
}

OCTO_PUBLIC
void application_destroy(Application* app) {
	LOGv("Destroying GLFW application...");

	Mem.destroy(app);

	LOGv("GLFW application destroyed");
}

}
