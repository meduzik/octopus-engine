#include "window.glfw.hpp"
#include "../render.gl/init.hpp"
#include <octopus/log/log.hpp>

namespace octo {

OCTO_PUBLIC
void surface_get_size(Surface* surface, i32* width, i32* height) {
	*width = surface->current_width;
	*height = surface->current_height;
}

OCTO_PUBLIC
f64 surface_get_pixel_density(Surface* surface) {
	return surface->pixel_density;
}

OCTO_PUBLIC
void surface_set_listener(Surface* surface, ISurfaceListener* listener) {
	if (surface->listener) {
		surface->listener->on_unbind(surface);
	}
	surface->listener = listener;
	if (listener) {
		listener->on_bind(surface);
	}
}

OCTO_PUBLIC
render::Device* surface_get_device(Surface* surface) {
	return surface->device;
}

void glfw_surface_init(Window* window, Surface* surface) {
	LOGv("Created surface $surf for window $win", {"surf", surface->id}, {"win", window->id});

	surface->shared_context = render::context_create();
	surface->device = render::device_create(surface->shared_context);
}

void glfw_surface_destroy(Window* window, Surface* surface) {
	LOGv("Destroyed surface $surf for window $win", { "surf", surface->id }, { "win", window->id });	
	if (surface->listener) {
		surface->listener->on_unbind(surface);
		surface->listener = nullptr;
	}
	if (surface->device) {
		render::device_destroy(surface->device);
		surface->device = nullptr;
	}
	if (surface->shared_context) {
		render::context_destroy(surface->shared_context);
		surface->shared_context = nullptr;
	}
}


void glfw_surface_on_window_resized(Surface* surface, i32 width, i32 height) {
	surface->window_width = width;
	surface->window_height = height;
	surface->valid = false;
}

void glfw_surface_on_framebuffer_resized(Surface* surface, i32 width, i32 height) {
	surface->buffer_width = width;
	surface->buffer_height = height;
	surface->valid = false;
}

bool glfw_surface_validate(Surface* surface) {
	if (surface->valid) {
		return true;
	}
	if ((!surface->window_width) || (!surface->window_height) || (!surface->buffer_width) || (!surface->buffer_height)) {
		return false;
	}
	bool changes = false;
	if (surface->current_width != surface->buffer_width) {
		surface->current_width = surface->buffer_width;
		changes = true;
	}
	if (surface->current_height != surface->buffer_height) {
		surface->current_height = surface->buffer_height;
		changes = true;
	}
	f64 pixel_density = std::max(
		(f64)surface->buffer_width / surface->window_width,
		(f64)surface->buffer_height / surface->window_height
	);
	if (pixel_density != surface->pixel_density) {
		surface->pixel_density = pixel_density;
		changes = true;		
	}
	LOGv("Surface $surf set to $w x $h @ $pxd", 
		{"surf", surface->id},
		{"w", surface->current_width},
		{"h", surface->current_height},
		{"pxd", pixel_density}
	);
	surface->listener->on_resized(surface, surface->current_width, surface->current_height, surface->pixel_density);
	surface->valid = true;
	return true;
}

bool glfw_surface_begin_render(Surface* surface) {
	if (!surface->valid) {
		return false;
	}
	return true;
}

void glfw_surface_render(Surface* surface) {
	surface->listener->on_render(surface, surface->device);
}

void glfw_surface_end_render(Surface* surface) {
}

Window* glfw_surface_get_window(Surface* surface) {
	return (Window*)((char*)surface - offsetof(Window, surface));
}

void glfw_surface_update(Surface* surface) {
	f64 new_time = glfwGetTime();
	if (surface->last_timestamp != 0.0) {
		f64 dt = new_time - surface->last_timestamp;
		surface->listener->on_update(surface, dt);
	}
	surface->last_timestamp = new_time;
}

void glfw_surface_skip_update(Surface* surface) {
	glfw_surface_update(surface);
}

}
