#pragma once
#include <octopus/app/window.hpp>
#include "surface.glfw.hpp"

namespace octo {


struct Window {
	bool created = false;
	u32 id = 0;
	GLFWwindow* window = nullptr;
	IWindowListener* listener = nullptr;
	Surface surface;
};

struct Application {
	Window main_window;
};


}
