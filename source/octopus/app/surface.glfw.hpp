#pragma once
#include <octopus/app/window.hpp>
#include "glfw.hpp"

namespace octo {

struct Window;

namespace render {
struct Context;
}

struct Surface {
	i32 current_width = 0, current_height = 0;
	f64 pixel_density = 1.0;
	bool valid = false;

	f64 last_timestamp = 0;

	i32 buffer_width = 0, buffer_height = 0;
	i32 window_width = 0, window_height = 0;
	u32 id = 0;
	ISurfaceListener* listener = nullptr;

	render::Context* shared_context = nullptr;
	render::Device* device = nullptr;

};

void glfw_surface_init(Window* window, Surface* surface);
void glfw_surface_destroy(Window* window, Surface* surface);

void glfw_surface_on_window_resized(Surface* surface, i32 width, i32 height);
void glfw_surface_on_framebuffer_resized(Surface* surface, i32 width, i32 height);

bool glfw_surface_validate(Surface* surface);

bool glfw_surface_begin_render(Surface* surface);
void glfw_surface_render(Surface* surface);
void glfw_surface_end_render(Surface* surface);

Window* glfw_surface_get_window(Surface* surface);

void glfw_surface_update(Surface* surface);
void glfw_surface_skip_update(Surface* surface);

}
