#pragma once

#include "windows.hpp"
#include <octopus/containers/string_ref.hpp>
#include <octopus/containers/span.hpp>
#include <octopus/memory/arena.hpp>

namespace octo::win {

OCTO_PUBLIC Span<u16> widen(Arena* arena, StringRef string);
OCTO_PUBLIC StringRef narrow(Arena* arena, Span<u16> string);

}
