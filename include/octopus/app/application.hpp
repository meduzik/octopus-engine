#pragma once

#include <octopus/core/types.hpp>

namespace octo {

struct Application;

OCTO_PUBLIC Application* application_create();
OCTO_PUBLIC void application_loop(Application* application);
OCTO_PUBLIC void application_destroy(Application* application);


}
