#pragma once

#include <octopus/macros/detect.h>

#if defined(OCTO_PLATFORM_WIN)
	#define OCTO_HAS_WINDOWS
#endif

#if defined(OCTO_HAS_WINDOWS)

#include "surface.hpp"

namespace octo {

struct Application;
struct Window;

struct WindowCreationParams {
	u32 width, height;
	StringRef title;

	WindowCreationParams() :
		title("Octopus Engine"),
		width(800),
		height(600) {
	}
};

struct IWindowListener {
	virtual void on_bind(Window* window) {
	}

	virtual void on_unbind(Window* window) {
	}

	// Called when focused state of the window changes
	virtual void on_focus(Window* window, bool gained) {
	}

	// Called when the user attempts to close your window
	// Return true to allow, and false to deny
	virtual bool on_close(Window* window) {
		return true;
	}
};

OCTO_PUBLIC
Window* window_create(Application* app, const WindowCreationParams* params, IWindowListener* listener);

OCTO_PUBLIC
Surface* window_get_surface(Window* win);

OCTO_PUBLIC
void window_set_mouse_capture(Window* win, bool capture);

OCTO_PUBLIC
void window_destroy(Window* win);

}
#endif