#pragma once

namespace octo {

struct Surface;

namespace render {
struct Device;
}

namespace KeyCode {
	inline constexpr u16 Esc = 1;

	inline constexpr u16 Q = 16;
	inline constexpr u16 W = 17;
	inline constexpr u16 E = 18;
	inline constexpr u16 R = 19;
	inline constexpr u16 T = 20;
	inline constexpr u16 Y = 21;
	inline constexpr u16 U = 22;
	inline constexpr u16 I = 23;
	inline constexpr u16 O = 24;
	inline constexpr u16 P = 25;
	inline constexpr u16 LBracket = 26;
	inline constexpr u16 RBracket = 27;
	inline constexpr u16 A = 30;
	inline constexpr u16 S = 31;
	inline constexpr u16 D = 32;
	inline constexpr u16 F = 33;
	inline constexpr u16 G = 34;
	inline constexpr u16 H = 35;
	inline constexpr u16 J = 36;
	inline constexpr u16 K = 37;
	inline constexpr u16 L = 37;
	inline constexpr u16 Colon = 38;
	inline constexpr u16 Quote = 39;
};

enum class ButtonState: u8 {
	Down,
	Up
};

enum class MouseButton: u8 {
	Left,
	Right,
	Middle,
	Extra0
};

struct ISurfaceListener {
public:
	virtual void on_bind(Surface* surface) {
	}

	virtual void on_unbind(Surface* surface) {
	}

	virtual void on_update(Surface* surface, f64 dt) {
	}

	virtual void on_render(Surface* surface, render::Device* device) {
	}

	virtual void on_resized(Surface* surface, i32 width, i32 height, f64 pixel_density) {
	}

	virtual void on_key_press(Surface* surface, u16 keycode, ButtonState state) {
	}

	virtual void on_mouse_enter(Surface* surface) {
	}

	virtual void on_mouse_leave(Surface* surface) {
	}

	virtual void on_mouse_move(Surface* surface, f64 x, f64 y) {
	}
	
	virtual void on_scroll(Surface* surface, f64 x_offset, f64 y_offset) {
	}

	virtual void on_mouse_press(Surface* surface, MouseButton button, ButtonState state) {
	}
};

OCTO_PUBLIC
void surface_set_listener(Surface* surface, ISurfaceListener* listener);

// Returns the number of pixels on both dimensions of the surface
OCTO_PUBLIC
void surface_get_size(Surface* surface, i32* width, i32* height);


// Returns pixel density for the surface
// Pixel density is something very device dependent.
// It should be reported as 2 for hidpi displays, and 1 for regular displays.
OCTO_PUBLIC
f64 surface_get_pixel_density(Surface* surface);

OCTO_PUBLIC
render::Device* surface_get_device(Surface* surface);

}

