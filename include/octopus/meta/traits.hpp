#pragma once

namespace octo::meta {

template<typename T>
constexpr bool IsTriviallyRelocatable = std::is_trivially_copyable_v<T>;

template<typename T>
constexpr bool IsZeroConstructible = std::is_trivially_constructible_v<T>;

template<typename T>
constexpr bool HasUniqueRepresentation = std::has_unique_object_representations<T>;


}