#pragma once

namespace octo::meta {

namespace impl {
template<bool>
struct Select;

template<>
struct Select<0> {
	template<typename, typename T>
	using X = T;
};

template<>
struct Select<1> {
	template<typename T, typename>
	using X = T;
};
}


template<bool TCondition, typename T1, typename T0>
using Select = typename impl::Select<TCondition>::template X<T1, T0>;

}