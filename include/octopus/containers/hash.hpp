#pragma once

#include <octopus/meta/traits.hpp>
#include <octopus/utils/cityhash.hpp>

namespace octo {

template<typename TState, typename T>
TState HashExt(TState, T) = delete;

template<typename TState>
octo_forceinline TState HashExt(TState state, bool value) {
	return state.Combine((u8)value);
}

template<typename TState, typename T>
octo_forceinline TState HashExt(TState state, T* value) {
	uword integer = (uword)value;
	return state.Combine(integer, integer);
}

template<typename TState, typename TEnum>
octo_forceinline auto HashExt(TState state, TEnum value)
-> std::enable_if_t<std::is_enum_v<TState>, TState> {
	return state.Combine((std::underlying_type_t<TEnum>)value);
}

template<typename TState>
octo_forceinline TState HashExt(TState state, StringRef value) {
	return state.CombineBytes(value.data(), value.size());
}

template<typename TState, typename T>
octo_forceinline auto HashExt(TState state, const T& value)
-> std::enable_if_t<octo::meta::HasUniqueRepresentation<T>, TState> {
	if constexpr (sizeof(T) <= 8) {
		u64 bits = 0;
		memcpy(&bits, &value, sizeof(value));
		return state.Combine(bits);
	} else {
		return state.CombineBytes((const void*)&value, sizeof(value));
	}
}

template<typename TState, typename T>
octo_forceinline TState Hash(TState state, T val) {
	return HashExt(state, val);
}

using DefaultHash = CityHash;

template<typename THash>
struct HashState {
	static_assert(
		std::is_trivial_v<typename THash::StateType>,
		"hash state must be trivial");

	typename THash::StateType m_state;

	template<typename T>
	octo_forceinline HashState Combine(const T& value) {
		if constexpr (std::is_integral_v<T>) {
			return { THash::Combine(m_state, value) };
		} else {
			return Hash(*this, value);
		}
	}

	template<typename... TArgs>
	octo_forceinline HashState Combine(const TArgs&... args) {
		HashState state = *this;
		((state = state.Combine(args)), ...);
		return state;
	}

	template<typename T>
	octo_forceinline HashState CombineArray(const T* data, iword size) {
		if constexpr (meta::HasUniqueRepresentation<T>) {
			return CombineBytes(data, (uword)size * sizeof(T));
		} else {
			HashState state = *this;
			for (iword i = 0; i < size; ++i)
				state = state.Combine(data[i]);
			return state;
		}
	}

	octo_forceinline HashState CombineBytes(const void* data, uword size) {
		return { THash::Combine(m_state, data, size) };
	}
};

template<typename THash>
struct BasicHasher {
	template<typename T>
	static octo_noinline uword Hash(const T& value) {
		HashState<THash> state{ THash::Init() };
		return THash::Finalize(state.Combine(value).m_state);
	}
};

namespace ext {

template<typename T>
struct Hasher {
	using Type = BasicHasher<DefaultHash>;
};

}

template<typename T>
using Hasher = typename ext::Hasher<T>::Type;

}
