#pragma once

#include <octopus/meta/meta.hpp>

namespace octo::impl {

template<typename T>
using ConstructType = meta::Select<meta::IsZeroConstructible<T>, u8, T>;

template<typename T>
using DestroyType = meta::Select<std::is_trivially_destructible_v<T>, u8, T>;

template<typename T>
using CopyType = meta::Select<std::is_trivially_copyable_v<T>, u8, T>;

}
