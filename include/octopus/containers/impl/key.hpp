#pragma once

#include <octopus/macros/macros.h>

namespace octo {
namespace ext {

template<typename TKey>
octo_forceinline const TKey& SimplifyKey(const TKey& key) {
	return key;
}

}

template<typename T>
using SimpleKey = std::decay_t<decltype(ext::SimplifyKey(std::declval<T>()))>;

}
