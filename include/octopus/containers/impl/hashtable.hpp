#pragma once

#include <octopus/math/intrin.hpp>
#include <octopus/meta/select.hpp>
#include <octopus/meta/traits.hpp>
#include <octopus/math/intlog.hpp>
#include "traits.hpp"
#include "key.hpp"


namespace octo::impl::hashtable {


template<typename TKey, typename TInKey>
octo_forceinline decltype(auto) SimplifyKey(const TInKey& inKey)
{
	if constexpr (std::is_same_v<TKey, TInKey>)
	{
		return inKey;
	}
	else
	{
		decltype(auto) key = octo::ext::SimplifyKey(inKey);
		typedef std::decay_t<decltype(key)> SimpleKeyType;

		return static_cast<TKey>(key);
	}
}

enum Ctrl : i8
{
	Ctrl_Empty = (i8)0b10000000,
	Ctrl_Tomb  = (i8)0b11111110,
	Ctrl_End   = (i8)0b11111111,
};

constexpr iword GroupSize = 16;
extern const Ctrl EmptyGroup[];

//ARCH: SSE2
struct Group
{
	octo_forceinline explicit Group(const Ctrl* ctrl)
	{
		m_ctrl = _mm_loadu_si128((const __m128i*)ctrl);
	}

	octo_forceinline u32 Match(Ctrl h2) const
	{
		return _mm_movemask_epi8(_mm_cmpeq_epi8(_mm_set1_epi8(h2), m_ctrl));
	}

	octo_forceinline u32 MatchEmpty() const
	{
		//ARCH: SSSE3
#if OCTO_SSSE3
		return _mm_movemask_epi8(_mm_sign_epi8(m_ctrl, m_ctrl));
#else
		return Match(Ctrl_Empty);
#endif
	}

	octo_forceinline u32 MatchFree() const
	{
		return _mm_movemask_epi8(_mm_cmpgt_epi8(
			_mm_set1_epi8(Ctrl_End), m_ctrl));
	}

	octo_forceinline u32 CountLeadingFree() const
	{
		__m128i end = _mm_set1_epi8(Ctrl_End);
		u32 mask = _mm_movemask_epi8(_mm_cmpgt_epi8(end, m_ctrl));
		return mask == 0 ? GroupSize : octo_ctz32(mask + 1);
	}

	__m128i m_ctrl;
};

struct Probe
{
	octo_forceinline Probe(uword hash, uword mask)
	{
		m_mask = mask;
		m_offset = hash & mask;
		m_index = 0;
	}

	octo_forceinline iword Offset(iword offset)
	{
		return (m_offset + offset) & m_mask;
	}

	octo_forceinline void Next()
	{
		m_index += GroupSize;
		m_offset += m_index;
		m_offset &= m_mask;
	}

	uword m_mask;
	uword m_offset;
	uword m_index;
};

#define OCTO_HT_HASH1(hash) ((hash) >> 7)
#define OCTO_HT_HASH2(hash) ((Ctrl)((hash) & 0x7F))

#define OCTO_HT_SLOTS(ctrl, capacity) ((u8*)((ctrl) + (capacity) + 1 + GroupSize))


struct IteratorCore
{
	Ctrl* m_ctrl;
	u8* m_slot;

	template<uword TElementSize>
	void SkipFreeSlots()
	{
		Ctrl* ctrl = m_ctrl;
		u8* slot = m_slot;

		while (*ctrl < Ctrl_End)
		{
			u32 shift = Group(ctrl).CountLeadingFree();

			ctrl += shift;
			slot += shift * TElementSize;
		}

		m_ctrl = ctrl;
		m_slot = slot;
	}

	template<uword TElementSize>
	void Advance()
	{
		m_ctrl += 1;
		m_slot += TElementSize;
		SkipFreeSlots<TElementSize>();
	}

	template<uword TElementSize>
	static IteratorCore Begin(Ctrl* ctrl, iword capacity)
	{
		IteratorCore iterator;
		iterator.m_ctrl = ctrl;
		iterator.m_slot = OCTO_HT_SLOTS(ctrl, capacity);
		iterator.SkipFreeSlots<TElementSize>();
		return iterator;
	}

	static IteratorCore End(Ctrl* ctrl, iword capacity)
	{
		IteratorCore iterator;
		iterator.m_ctrl = ctrl + capacity;
		return iterator;
	}
};

template<typename T>
class BasicIterator : IteratorCore
{
public:
	octo_forceinline BasicIterator(IteratorCore iterator)
		: IteratorCore(iterator)
	{
	}

	octo_forceinline T& operator*() const
	{
		return *(T*)m_slot;
	}

	octo_forceinline T* operator->() const
	{
		return (T*)m_slot;
	}

	octo_forceinline BasicIterator& operator++()
	{
		Advance<sizeof(T)>();
		return *this;
	}

	octo_forceinline BasicIterator operator++(int)
	{
		auto it = *this;
		Advance<sizeof(T)>();
		return it;
	}

	octo_forceinline bool operator==(const BasicIterator& other) const
	{
		return m_ctrl == other.m_ctrl;
	}

	octo_forceinline bool operator!=(const BasicIterator& other) const
	{
		return m_ctrl != other.m_ctrl;
	}
};


struct Core
{
	Ctrl* ctrl;
	iword size;
	iword free;
	iword capacity;
};

constexpr octo_forceinline uword GetBufferSize(iword capacity, uword elemSize)
{
	return (uword)(capacity + 1 + GroupSize) + elemSize * (uword)capacity;
}

constexpr octo_forceinline iword GetMaxSize(iword capacity)
{
	return capacity - (iword)((uword)capacity / 8);
}

template<bool THasLocalStorage>
octo_forceinline bool IsDynamic(Core* table, Ctrl* ctrl)
{
	if constexpr (THasLocalStorage)
	{
		return ctrl != (Ctrl*)(table + 1);
	}
	else
	{
		return ctrl != EmptyGroup;
	}
}

octo_forceinline void InitCtrl(Ctrl* ctrl, iword capacity) {
	uword ctrlSize = capacity + 1 + GroupSize;
	octo_assert(ctrlSize % GroupSize == 0);
	memset(ctrl, Ctrl_Empty, ctrlSize);
	ctrl[capacity] = Ctrl_End;
}

template<bool THasLocalStorage>
void ConstructTable(Core* table, iword localCapacity)
{
	if constexpr (THasLocalStorage)
	{
		Ctrl* ctrl = (Ctrl*)(table + 1);
		InitCtrl(ctrl, localCapacity);

		table->ctrl = ctrl;
		table->size = 0;
		table->free = GetMaxSize(localCapacity);
		table->capacity = localCapacity;
	}
	else
	{
		table->ctrl = (Ctrl*)EmptyGroup;
		table->size = 0;
		table->free = 0;
		table->capacity = 0;
	}
}

template<typename TDestroy, typename TAlloc, bool THasLocalStorage>
void DestroyTable(Core* table, uword elemSize, TAlloc alloc)
{
	Ctrl* ctrl = table->ctrl;
	iword capacity = table->capacity;

	if constexpr (!std::is_trivially_destructible_v<TDestroy>)
	{
		if (table->size > 0)
		{
			u8* slot = OCTO_HT_SLOTS(ctrl, capacity);
			for (iword slotIndex = 0; slotIndex < capacity;
				++slotIndex, slot += elemSize)
			{
				if (ctrl[slotIndex] >= 0)
					std::destroy_at((TDestroy*)slot);
			}
		}
	}

	if (IsDynamic<THasLocalStorage>(table, ctrl))
	{
		alloc.free((u8*)ctrl, GetBufferSize(capacity, elemSize));
	}
}

struct KeyTraits
{
	template<typename TKey, typename T>
	static octo_forceinline const TKey* GetKey(const T* slot)
	{
		return (const TKey*)slot;
	}
};

octo_forceinline void SetCtrl(Ctrl* ctrl,
	iword capacity, iword slotIndex, Ctrl h2)
{
	ctrl[slotIndex] = h2;
	ctrl[(slotIndex - GroupSize & capacity) + GroupSize] = h2;
}

inline iword FindFreeSlot(const Core* core, uword hash)
{
	Ctrl* ctrl = core->ctrl;
	iword capacity = core->capacity;

	//TODO: move args ops in?
	Probe probe(OCTO_HT_HASH1(hash), (uword)capacity);

	while (true)
	{
		Group group(ctrl + probe.m_offset);

		if (u32 mask = group.MatchFree())
			return probe.Offset(octo_ctz32(mask));

		octo_assert(probe.m_index < (uword)capacity);
		probe.Next();
	}
}

template<typename T>
struct InsertResult
{
	T* Element;
	bool Inserted;
};

typedef InsertResult<void> InsertCallback(
	Core* core, uword elemSize, uword hash, const void* context);

//typedef FunctionRef<InsertCallbackSignature> InsertCallback2;

OCTO_PUBLIC
InsertResult<void> FailCallback(Core* core,
	uword elemSize, uword hash, const void* context);

OCTO_PUBLIC
InsertResult<void> InsertSlot(Core* table,
	uword elemSize, uword hash, iword slotIndex);

OCTO_PUBLIC
InsertResult<void> InsertWithCallback(
	Core* table, uword elemSize, uword hash,
	InsertCallback insertCallback, const void* insertContext);

OCTO_PUBLIC
void RemoveSlot(Core* table, iword slotIndex);

OCTO_PUBLIC
void ConvertTombToEmptyAndFullToTomb(Ctrl* ctrl, iword capacity);

octo_forceinline iword GetProbeIndex(
	iword slotIndex, uword hash, iword capacity)
{
	uword offset = (uword)slotIndex - OCTO_HT_HASH1(hash) & (uword)capacity;
	return (iword)(offset / (uword)GroupSize);
}

template<typename TKey, typename TKeyTraits, typename TCompare, typename TInKey>
octo_noinline
void* Find(const Core* table, uword elemSize, uword hash, TInKey key)
{
	Ctrl* ctrl = table->ctrl;
	iword capacity = table->capacity;
	u8* slots = OCTO_HT_SLOTS(ctrl, capacity);

	Probe probe(OCTO_HT_HASH1(hash), (uword)capacity);

	for (Ctrl h2 = OCTO_HT_HASH2(hash);;)
	{
		Group group(ctrl + probe.m_offset);

		for (u32 mask = group.Match(h2); mask != 0; mask &= mask - 1)
		{
			u8* slot = slots + elemSize * probe.Offset(octo_ctz32(mask));

			if (octo_likely(TCompare::Compare(key,
				ext::SimplifyKey<SimpleKey<TKey>>(
					*TKeyTraits::template GetKey<TKey>(slot)))))
			{
				return slot;
			}
		}

		if (octo_likely(group.MatchEmpty()))
			return nullptr;

		probe.Next();
	}
}

template<typename TKey, typename TKeyTraits, typename THash>
static void Rehash(Core* dst, Core* src, uword elemSize)
{
	Ctrl* dstCtrl = dst->ctrl;
	iword dstCapacity = dst->capacity;
	u8* dstSlots = OCTO_HT_SLOTS(dstCtrl, dstCapacity);

	Ctrl* srcCtrl = src->ctrl;
	iword srcCapacity = src->capacity;

	u8* srcSlot = OCTO_HT_SLOTS(srcCtrl, srcCapacity);
	for (iword srcSlotIndex = 0; srcSlotIndex < srcCapacity;
		++srcSlotIndex, srcSlot += elemSize)
	{
		if (srcCtrl[srcSlotIndex] >= 0)
		{
			uword hash = THash::Hash(
				ext::SimplifyKey<SimpleKey<TKey>>(
					*TKeyTraits::template GetKey<TKey>(srcSlot)));

			iword dstSlotIndex = FindFreeSlot(dst, hash);
			SetCtrl(dstCtrl, dstCapacity, dstSlotIndex, OCTO_HT_HASH2(hash));

			u8* dstSlot = dstSlots + elemSize * dstSlotIndex;
			memcpy(dstSlot, srcSlot, elemSize);
		}
	}
}

template<typename TKey, typename TKeyTraits,
	typename THash, typename TAllocatorHandle, bool THasLocalStorage>
static void Resize(Core* table, uword elemSize, iword newCapacity, TAllocatorHandle allocator)
{
	octo_assert(newCapacity > 0 && (newCapacity & newCapacity + 1) == 0);
	octo_assert(GetMaxSize(newCapacity) >= table->size);

	Ctrl* ctrl = (Ctrl*)allocator.allocate(GetBufferSize(newCapacity, elemSize));

	InitCtrl(ctrl, newCapacity);

	iword size = table->size;

	Core newTable;
	newTable.ctrl = ctrl;
	newTable.size = size;
	newTable.free = GetMaxSize(newCapacity) - size;
	newTable.capacity = newCapacity;

	if (size > 0)
	{
		Rehash<TKey, TKeyTraits, THash>(&newTable, table, elemSize);
	}

	ctrl = table->ctrl;
	if (IsDynamic<THasLocalStorage>(table, ctrl))
	{
		allocator.free((u8*)ctrl, GetBufferSize(table->capacity, elemSize));
	}

	*table = newTable;
}

template<typename TKey, typename TKeyTraits, typename THash>
static void ReuseTombs(Core* table, uword elemSize)
{
	Ctrl* ctrl = table->ctrl;
	iword capacity = table->capacity;

	ConvertTombToEmptyAndFullToTomb(ctrl, capacity);

	u8* slots = OCTO_HT_SLOTS(ctrl, capacity);

	u8* slot = slots;
	for (iword slotIndex = 0; slotIndex < capacity; ++slotIndex, slot += elemSize)
	{
		if (ctrl[slotIndex] != Ctrl_Tomb)
			continue;

		uword hash = THash::Hash(
			ext::SimplifyKey<SimpleKey<TKey>>(
				*TKeyTraits::template GetKey<TKey>(slot)));

		iword newSlotIndex = FindFreeSlot(table, hash);

		iword probeIndex = GetProbeIndex(slotIndex, hash, capacity);
		iword newProbeIndex = GetProbeIndex(newSlotIndex, hash, capacity);

		if (octo_likely(newProbeIndex == probeIndex))
		{
			SetCtrl(ctrl, capacity, slotIndex, OCTO_HT_HASH2(hash));
			continue;
		}

		u8* newSlot = slots + elemSize * newSlotIndex;
		if (ctrl[newSlotIndex] == Ctrl_Empty)
		{
			SetCtrl(ctrl, capacity, newSlotIndex, OCTO_HT_HASH2(hash));
			memcpy(newSlot, slot, elemSize);
			SetCtrl(ctrl, capacity, slotIndex, Ctrl_Empty);
		}
		else
		{
			octo_assert(ctrl[newSlotIndex] == Ctrl_Tomb);
			SetCtrl(ctrl, capacity, newSlotIndex, OCTO_HT_HASH2(hash));

			u8 temp[256];
			memcpy(temp, newSlot, elemSize);
			memcpy(newSlot, slot, elemSize);
			memcpy(slot, temp, elemSize);

			--slotIndex;
		}
	}
}

template<typename TKey, typename TKeyTraits,
	typename THash, typename TAllocatorHandle, bool THasLocalStorage>
static InsertResult<void> GrowCallback(Core* table,
	uword elemSize, uword hash, const void* allocator)
{
	iword capacity = table->capacity;
	if (capacity == 0)
	{
		Resize<TKey, TKeyTraits, THash, TAllocatorHandle, THasLocalStorage>(
			table, elemSize, GroupSize - 1, *(const TAllocatorHandle*)allocator);
	}
	else if (table->size <= GetMaxSize(capacity) / 2)
	{
		ReuseTombs<TKey, TKeyTraits, THash>(table, elemSize);
	}
	else
	{
		Resize<TKey, TKeyTraits, THash, TAllocatorHandle, THasLocalStorage>(
			table, elemSize, capacity * 2 + 1, *(const TAllocatorHandle*)allocator);
	}

	--table->free;

	iword slotIndex = FindFreeSlot(table, hash);
	return InsertSlot(table, elemSize, hash, slotIndex);
}

template<typename TKey, typename TKeyTraits, typename TCompare, typename TInKey>
static octo_noinline InsertResult<void> Insert(
	Core* table, uword elemSize, uword hash, TInKey key,
	InsertCallback insertCallback, const void* insertContext)
{
	Ctrl* ctrl = table->ctrl;
	iword capacity = table->capacity;
	u8* slots = OCTO_HT_SLOTS(ctrl, capacity);

	Probe probe(OCTO_HT_HASH1(hash), (uword)capacity);

	for (Ctrl h2 = OCTO_HT_HASH2(hash);;)
	{
		Group group(ctrl + probe.m_offset);

		for (u32 mask = group.Match(h2); mask != 0; mask &= mask - 1)
		{
			u8* slot = slots + elemSize *
				probe.Offset(octo_ctz32(mask));

			if (octo_likely(TCompare::Compare(key,
				ext::SimplifyKey<SimpleKey<TKey>>(
					*TKeyTraits::template GetKey<TKey>(slot)))))
			{
				return { slot, false };
			}
		}

		if (octo_likely(group.MatchEmpty()))
		{
			return InsertWithCallback(table,
				elemSize, hash, insertCallback, insertContext);
		}

		probe.Next();
	}
}
	
//TODO: make sure the elem is destroyed on the outside...
template<typename TKey, typename TKeyTraits, typename TCompare, typename TInKey>
static void* Remove(Core* table, uword elemSize, uword hash, TInKey key)
{
	void* slot = Find<
		TKey, TKeyTraits, TCompare, TInKey>(
			table, elemSize, hash, key);

	if (slot != nullptr)
	{
		//TODO: do something cleverer than integer division
		RemoveSlot(table, ((u8*)slot - OCTO_HT_SLOTS(
			table->ctrl, table->capacity)) / elemSize);
	}

	return slot;
}

template<typename TDestroy>
void Clear(Core* table, uword elemSize)
{
	iword capacity = table->capacity;
	iword maxSize = GetMaxSize(capacity);

	if (table->free < maxSize)
	{
		Ctrl* ctrl = table->ctrl;
		if constexpr (!std::is_trivially_destructible_v<TDestroy>)
		{
			if (table->size > 0)
			{
				u8* slot = OCTO_HT_SLOTS(ctrl, capacity);
				for (iword slotIndex = 0; slotIndex < capacity;
					++slotIndex, slot += elemSize)
				{
					if (ctrl[slotIndex] >= 0)
						Destroy((TDestroy*)slot);
				}
			}
		}
		InitCtrl(ctrl, capacity);

		table->size = 0;
		table->free = maxSize;
	}
}



template<typename TKey, typename TKeyTraits,
	typename THash, typename TAllocatorHandle, bool THasLocalStorage>
static void Reserve(Core* table, uword elemSize, iword minCapacity, TAllocatorHandle allocator)
{
	if (table->capacity < minCapacity) {
		uword po2 = 1;
		uword target_cap = minCapacity + 1;
		while (po2 < target_cap) {
			po2 = po2 << (uword)1;
		}
		Resize<TKey, TKeyTraits, THash, TAllocatorHandle, THasLocalStorage>(
			table, elemSize, (iword)po2 - 1, allocator);
	}
}

struct DefaultCompare
{
	template<typename T1, typename T2>
	static octo_forceinline bool Compare(const T1& t1, const T2& t2)
	{
		return t1 == t2;
	}
};


template<typename TCore, typename T, iword TCapacity,
	bool = (TCapacity > 0) && (alignof(T) > alignof(TCore))>
struct StorageLayer;

template<typename TCore, typename T>
struct StorageLayer<TCore, T, 0, 0>
{
	TCore core;
};

template<typename TCore, typename T, iword TCapacity>
struct StorageLayer<TCore, T, TCapacity, 0>
{
	TCore core;
	std::aligned_storage_t<
		GetBufferSize(TCapacity, sizeof(T)),
		alignof(T)
	> mutable storage;
};

template<typename TCore, typename T, iword TCapacity>
struct StorageLayer<TCore, T, TCapacity, 1>
{
	std::aligned_storage_t<
		alignof(T) - alignof(TCore),
		1
	> padding;

	TCore core;
	std::aligned_storage_t<
		GetBufferSize(TCapacity, sizeof(T)),
		alignof(T)
	> mutable storage;
};

template<typename TStorage, typename TAllocatorHandle>
struct AllocatorLayer : TAllocatorHandle, TStorage
{
	AllocatorLayer() = default;

	octo_forceinline AllocatorLayer(const TAllocatorHandle& allocator)
		: TAllocatorHandle(allocator)
	{
	}
};

template<uword TSize>
struct TrivialKey
{
	//TODO: compute minimum alignment
	std::aligned_storage_t<TSize, 1> m_buffer;

	octo_forceinline bool operator==(const TrivialKey& other)
	{
		return memcmp(m_buffer, other.m_buffer, TSize) == 0;
	}
};

template<typename TCompare, typename TKey>
using CompareType = meta::Select<
	std::is_same_v<TCompare, DefaultCompare> &&
	meta::HasUniqueRepresentation<TKey>, TrivialKey<sizeof(TKey)>, TKey>;

template<
	typename T, typename TKey, typename TKeyTraits, typename THash,
	typename TCompare, typename TAllocatorHandle, iword TCapacity>
struct HashTable
{
	static_assert(octo::meta::IsTriviallyRelocatable<T>);

	static_assert(sizeof(T) <= 256, "See ReuseTombs implementation");

	static constexpr iword LocalCapacity =
		TCapacity > 0
		?
		(iword)comptime::round_up_to_pot((uword)std::max(TCapacity, GroupSize)) - 1
		:
		0
	;

	static_assert(TCapacity == 0 || LocalCapacity + 1 >= alignof(T));

public:
	typedef BasicIterator<      T> Iterator;
	typedef BasicIterator<const T> ConstIterator;


	HashTable()
	{
		ConstructTable<(TCapacity > 0)>(&m.core, { LocalCapacity });
	}

	HashTable(TAllocatorHandle allocator)
		: m(std::move(allocator))
	{
		ConstructTable<(TCapacity > 0)>(&m.core, { LocalCapacity });
	}

	HashTable(HashTable&& src);
	HashTable& operator=(HashTable&& src);

	HashTable(const HashTable&) = delete;
	HashTable& operator=(const HashTable&) = delete;

	~HashTable()
	{
		DestroyTable<DestroyType<T>, TAllocatorHandle, (TCapacity > 0)>(
			&m.core, sizeof(T), static_cast<const TAllocatorHandle&>(m));
	}


	octo_forceinline iword Size() const
	{
		return m.core.size;
	}

	octo_forceinline iword Capacity() const
	{
		return m.core.capacity;
	}

	octo_forceinline Iterator begin()
	{
		return IteratorCore::Begin<sizeof(T)>(m.core.ctrl, m.core.capacity);
	}

	octo_forceinline ConstIterator begin() const
	{
		return IteratorCore::Begin<sizeof(T)>(m.core.ctrl, m.core.capacity);
	}

	octo_forceinline Iterator end()
	{
		return IteratorCore::End(m.core.ctrl, m.core.capacity);
	}

	octo_forceinline ConstIterator end() const
	{
		return IteratorCore::End(m.core.ctrl, m.core.capacity);
	}

protected:
	template<typename TInKey>
	octo_forceinline T* Find(const TInKey& inKey) const
	{
		//TODO: collapse TCompare=DefaultCompare and TKey has unique repr to
		// dummy layout type

		decltype(auto) key = ext::SimplifyKey<SimpleKey<TKey>>(inKey);
		using KeyParamType = decltype(key);

		return (T*)impl::hashtable::Find<
			TKey, TKeyTraits, TCompare, KeyParamType>(
				&m.core, sizeof(T), THash::Hash(key), key);
	}

	template<typename TInKey>
	octo_forceinline InsertResult<T> Insert(const TInKey& inKey,
		InsertCallback insertCallback, const void* insertContext)
	{
		decltype(auto) key = ext::SimplifyKey<SimpleKey<TKey>>(inKey);
		using KeyParamType = decltype(key);

		auto result = impl::hashtable::Insert<TKey, TKeyTraits, TCompare, KeyParamType>(
				&m.core, sizeof(T), THash::Hash(key), key, insertCallback, insertContext);

		return InsertResult<T>{ (T*)result.Element, result.Inserted };
	}

	template<typename TInKey>
	octo_forceinline T* Remove(const TInKey& inKey)
	{
		decltype(auto) key = ext::SimplifyKey<SimpleKey<TKey>>(inKey);
		using KeyParamType = decltype(key);

		return (T*)impl::hashtable::Remove<
			TKey, TKeyTraits, TCompare, KeyParamType>(
				&m.core, sizeof(T), THash::Hash(key), key);
	}


	AllocatorLayer<StorageLayer<Core, T, LocalCapacity>, TAllocatorHandle> m;
};

#ifndef OCTO_HASH_TABLE_IMPL
	#undef OCTO_HT_SLOTS
	#undef OCTO_HT_HASH1
	#undef OCTO_HT_HASH2
#endif

}
