#pragma once

#include <octopus/meta/traits.hpp>

namespace octo {

template<typename TKey, typename TValue>
struct KeyValuePair
{
	typedef TKey KeyType;
	typedef TValue ValueType;

	TKey Key;
	TValue Value;

	explicit octo_forceinline KeyValuePair(TKey key)
		: Key(std::move(key))
	{
	}

	octo_forceinline KeyValuePair(TKey key, TValue value)
		: Key(std::move(key)), Value(std::move(value))
	{
	}
};


} 

template<typename TKey, typename TValue>
constexpr bool octo::meta::IsTriviallyRelocatable<octo::KeyValuePair<TKey, TValue>> =
octo::meta::IsTriviallyRelocatable<TKey> && meta::IsTriviallyRelocatable<TValue>;
