#pragma once
#include <octopus/macros/api.h>
#include "string_ref.hpp"

namespace octo {

struct OCTO_PUBLIC Twine {
	Twine(StringRef sr) :
		tag(twine_tag::string_ref),
		value(sr) {
	}

	Twine(const Twine* lhs, const Twine* rhs) :
		tag(twine_tag::concat_tw_tw),
		value(lhs, rhs) {
	}

	Twine(const Twine* lhs, StringRef rhs) :
		tag(twine_tag::concat_tw_sr),
		value(lhs, rhs) {
	}

	Twine(StringRef lhs, const Twine* rhs) :
		tag(twine_tag::concat_sr_tw),
		value(lhs, rhs) {
	}

	Twine(StringRef lhs, StringRef rhs) :
		tag(twine_tag::concat_sr_sr),
		value(lhs, rhs) {
	}

	uword size() const {
		switch (tag) {
		case twine_tag::empty: return 0;
		case twine_tag::string_ref: return value.sr.size();
		case twine_tag::concat_tw_tw: return value.concat_tt.lhs->size() + value.concat_tt.rhs->size();
		case twine_tag::concat_sr_tw: return value.concat_srt.lhs.size() + value.concat_srt.rhs->size();
		case twine_tag::concat_tw_sr: return value.concat_tsr.lhs->size() + value.concat_tsr.rhs.size();
		case twine_tag::concat_sr_sr: return value.concat_srsr.lhs.size() + value.concat_srsr.rhs.size();
		default: octo_unreachable;
		}
	}

	template<class Alc>
	StringRef allocate(Alc& alc) const {
		uword sz = size();
		u8* ptr = alc.allocate(sz);
		write(ptr, this);
		return StringRef(ptr, sz);
	}

	template<class Alc>
	static void free(Alc& alc, StringRef sr) {
		alc.free(sr.data(), sr.size());
	}

	template<class Fn>
	void visit(Fn&& fn) const {
		switch (tag) {
		case twine_tag::empty: {
		} break;
		case twine_tag::string_ref: {
			fn(value.sr);
		} break;
		case twine_tag::concat_tw_tw: {
			value.concat_tt.lhs->visit(fn);
			value.concat_tt.rhs->visit(fn);
		} break;
		case twine_tag::concat_sr_tw: {
			fn(value.concat_srt.lhs);
			value.concat_srt.rhs->visit(fn);
		} break;
		case twine_tag::concat_tw_sr: {
			value.concat_tsr.lhs->visit(fn);
			fn(value.concat_tsr.rhs);
		} break;
		case twine_tag::concat_sr_sr: {
			fn(value.concat_srsr.lhs);
			fn(value.concat_srsr.rhs);
		} break;
		default: octo_unreachable;
		}
	}
private:
	static u8* write(u8* buf, const Twine* tw);
	static u8* write(u8* buf, StringRef sr);

	enum class twine_tag {
		empty,
		string_ref,
		concat_sr_sr,
		concat_sr_tw,
		concat_tw_sr,
		concat_tw_tw,
	} tag;
	union OCTO_PUBLIC value_union {
		StringRef sr;
		struct {
			const Twine* lhs;
			const Twine* rhs;
		} concat_tt;
		struct {
			const Twine* lhs;
			StringRef rhs;
		} concat_tsr;
		struct {
			StringRef lhs;
			const Twine* rhs;
		} concat_srt;
		struct {
			StringRef lhs;
			StringRef rhs;
		} concat_srsr;

		constexpr value_union(StringRef sr) :
			sr(sr) {
		}
		constexpr value_union(const Twine* lhs, const Twine* rhs) :
			concat_tt{ lhs, rhs } {
		}
		constexpr value_union(StringRef lhs, const Twine* rhs) :
			concat_srt{ lhs, rhs } {
		}
		constexpr value_union(const Twine* lhs, StringRef rhs) :
			concat_tsr{ lhs, rhs } {
		}
		constexpr value_union(StringRef lhs, StringRef rhs) :
			concat_srsr{ lhs, rhs } {
		}
	} value;
};

inline octo_forceinline Twine operator+(const Twine& lhs, const Twine& rhs) {
	return Twine(&lhs, &rhs);
}

inline octo_forceinline Twine operator+(const Twine& lhs, StringRef rhs) {
	return Twine(&lhs, rhs);
}

inline octo_forceinline Twine operator+(StringRef lhs, const Twine& rhs) {
	return Twine(lhs, &rhs);
}

inline octo_forceinline Twine operator+(StringRef lhs, StringRef rhs) {
	return Twine(lhs, rhs);
}

}

