#pragma once
#include <octopus/macros/api.h>
#include <octopus/core/types.hpp>
#include "span.hpp"

namespace octo {

class OCTO_PUBLIC StringRef {
public:
	constexpr StringRef(std::nullptr_t) :
		_begin(nullptr),
		_size(0) {
	}

	constexpr StringRef(const u8* begin, uword size) :
		_begin(begin),
		_size(size) {
	}

	constexpr StringRef(const u8* begin, const u8* end) :
		_begin(begin),
		_size(end - begin) {
	}

	template<size_t N>
	constexpr StringRef(const char(&literal)[N]) :
		_begin((const u8*)literal),
		// note the -1 to account for the terminating \0
		_size(N - 1) {
	}

	StringRef(const char* c_string) :
		_begin((const u8*)c_string),
		_size(strlen(c_string)) {
	}

	constexpr StringRef(Span<u8> span) :
		_begin(span.data()),
		_size(span.size()) {
	}

	constexpr const u8* data() const {
		return _begin;
	}

	constexpr const u8* begin() const {
		return _begin;
	}

	constexpr const u8* end() const {
		return _begin + _size;
	}

	constexpr const size_t size() const {
		return _size;
	}

	constexpr u8 operator[](uword index) const {
		octo_assert(index < _size);
		return _begin[index];
	}

	constexpr Span<u8> to_span() const {
		return { _begin, _size };
	}

	bool operator==(StringRef other) const {
		if (_size != other._size) {
			return false;
		}
		if (_size == 0) {
			return true;
		}
		return !memcmp(_begin, other._begin, _size);
	}

	template<class Alc>
	char* copy_null_terminated(Alc& alc) const {
		char* buf = (char*)alc.allocate(_size + 1);
		memcpy(buf, _begin, _size);
		buf[_size] = 0;
		return buf;
	}
private:
	const u8* _begin;
	uword _size;
};

}

