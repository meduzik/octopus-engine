#pragma once

#include <octopus/core/std.hpp>
#include <octopus/memory/allocator.hpp>
#include <octopus/memory/arena.hpp>
#include <octopus/memory/pool.hpp>
#include <octopus/meta/traits.hpp>

namespace octo {


template <class T, class Alc>
class Vector: private Alc {
public:
	Vector() :
		Alc(),
		_begin(nullptr),
		_end(nullptr),
		_capacity(nullptr) {
	}

	Vector(const Alc& alc) :
		Alc(alc),
		_begin(nullptr),
		_end(nullptr),
		_capacity(nullptr) 
	{
	}

	Vector(Alc&& alc) : 
		Alc(std::move(alc)),
		_begin(nullptr),
		_end(nullptr),
		_capacity(nullptr)
	{
	}

	Vector(Vector<T, Alc>&& other) :
		Alc(std::move(other)),
		_begin(other._begin),
		_end(other._end),
		_capacity(other._capacity)
	{
		other._begin = other._end = other._capacity = nullptr;
	}

	octo_noinline
	~Vector() {
		if (_begin) {
			Alc::free((u8*)_begin, (_capacity - _begin) * sizeof(T));
		}
	}

	T& operator[](uword idx) {
		octo_assert(idx <= size());
		return *(_begin + idx);
	}

	const T& operator[](uword idx) const {
		octo_assert(idx <= size());
		return *(_begin + idx);
	}

	template<class... Args>
	T& push_back(Args&&... args) {
		T* val = make_slot();
		new(val)T(std::forward<Args>(args)...);
		return *val;
	}

	void reserve(uword new_cap) {
		if (new_cap > capacity()) {
			reallocate(new_cap);
		}
	}

	void resize(uword new_size) {
		if (size() == new_size) {
			return;
		} else if (size() < new_size) {
			reserve(new_size);
			auto old_end = _end;
			_end = _begin + new_size;
			for (auto ptr = old_end; ptr != _end; ptr++) {
				new(ptr)T();
			}
		} else {
			auto old_end = _end;
			_end = _begin + new_size;
			for (auto ptr = _end; ptr != old_end; ptr++) {
				std::destroy_at(ptr);
			}
		}
	}

	T& back() {
		octo_assert(_end != _begin);
		return *(_end - 1);
	}

	T* begin() {
		return _begin;
	}

	T* end() {
		return _end;
	}

	T* data() {
		return _begin;
	}

	const T* data() const {
		return _begin;
	}

	const T* begin() const {
		return _begin;
	}

	const T* end() const {
		return _end;
	}

	bool empty() const {
		return _begin == _end;
	}

	operator Span<T>() const {
		return {_begin, _end};
	}

	operator MutSpan<T>() {
		return { _begin, _end };
	}

	void pop_back() {
		octo_assert(_begin != _end);
		_end--;
	}

	uword size() const {
		return _end - _begin;
	}

	uword capacity() const {
		return _capacity - _begin;
	}
private:
	T* make_slot() {
		if (_end != _capacity) {
			return _end++;
		}
		return make_slot_reallocate();
	}

	octo_noinline
	void reallocate(uword min_cap) {
		uword old_cap = capacity();
		uword new_cap = std::max((uword)16, std::max(min_cap, old_cap * 3 / 2));
		T* new_begin;
		if (_begin) {
			if constexpr (meta::IsTriviallyRelocatable<T>) {
				new_begin = (T*)Alc::reallocate((u8*)_begin, _capacity - _begin, new_cap * sizeof(T));
			} else {
				new_begin = (T*)Alc::allocate(new_cap * sizeof(T));
				auto end_p = _end;
				auto out_p = new_begin;
				for (auto in_p = _begin; in_p != end_p; in_p++, out_p++) {
					new(out_p)T(std::move(*in_p));
				}
				for (auto in_p = _begin; in_p != end_p; in_p++) {
					std::destroy_at(in_p);
				}
				Alc::free((u8*)_begin, _capacity - _begin);
			}
			_end = new_begin + (_end - _begin);
		} else {
			new_begin = (T*)Alc::allocate(new_cap * sizeof(T));
			_end = new_begin;
		}
		_begin = new_begin;
		_capacity = _begin + new_cap;
	}

	octo_noinline
	T* make_slot_reallocate() {
		reallocate(capacity() + 1);
		return _end++;
	}

	T* _begin;
	T* _end;
	T* _capacity;
};



template<class T>
using ArenaVector = Vector< T, AllocatorRef<Arena> >;

template<class T>
using MemVector = Vector< T, Mallocator >;

/*
template<class T>
using PoolVector = std::vector< T, AllocatorStdRef< MemoryPool, T > >;


template<class T>
using DynamicVector = std::vector< T, AllocatorStdRef< DynamicAllocator, T > >;
*/
}

