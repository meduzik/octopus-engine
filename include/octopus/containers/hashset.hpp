#pragma once

// TODO::
/*
#include "xGame/Containers/KeyValuePair.hpp"
#include "xGame/Core.hpp"
#include "xGame/Debug/Assert.hpp"
#include "xGame/Memory/DefaultAllocator.hpp"
#include "xGame/MP/Default.hpp"
#include "xGame/Private/Containers/HashTable.hpp"
#include "xGame/Util/Default.hpp"

#include <utility>

namespace xGame {
namespace _::HashSet_ {

#if xGame_IDE && 0

template<typename T, typename THash,
	typename TCompare, typename TAllocator, iword TCapacity>
	class HashSet
{
public:
	typedef T ValueType;

	typedef       ValueType* Iterator;
	typedef const ValueType* ConstIterator;

	struct InsertResult
	{
		ValueType* Element;
		bool Inserted;
	};

	BasicAutoHashSet();
	BasicAutoHashSet(BasicAutoHashSet&&);
	BasicAutoHashSet(const BasicAutoHashSet&) = delete;
	BasicAutoHashSet& operator=(BasicAutoHashSet&&);
	BasicAutoHashSet& operator=(const BasicAutoHashSet&) = delete;

	iword Size() const;
	iword Capacity() const;


	template<typename TIn>
	bool Contains(const TIn& value) const;

	template<typename TIn>
	ValueType* Find(const TIn& value);

	template<typename TIn>
	const ValueType* Find(const TIn& value) const;


	template<typename TIn>
	InsertResult TryInsert(TIn&& value);

	template<typename TIn>
	InsertResult Insert(TIn&& value);

	template<typename TIn>
	InsertResult InsertUnsafe(TIn&& value);


	template<typename TIn>
	bool Remove(const TIn& value);

	void Clear();


	void Reserve(iword minCapacity);

	void ShrinkToFit();
};

#else

using namespace HashTable_;

template<typename T, typename THash,
	typename TCompare, typename TAllocatorHandle, iword TCapacity>
class HashSet : public HashTable<
	T, T, KeyTraits, THash, TCompare, TAllocatorHandle, TCapacity>
{
	typedef HashTable<T, T, KeyTraits, THash,
		TCompare, TAllocatorHandle, TCapacity> B;

public:
	typedef T ValueType;

	typedef InsertResult<T> InsertResult;


	using B::B;


	template<typename TIn>
	xGame_FORCEINLINE bool Contains(const TIn& value) const
	{
		return B::Find(value) != nullptr;
	}

	template<typename TIn>
	xGame_FORCEINLINE T* Find(const TIn& value)
	{
		return B::Find(value);
	}

	template<typename TIn>
	xGame_FORCEINLINE const T* Find(const TIn& value) const
	{
		return B::Find(value);
	}
	
	
	template<typename TIn>
	xGame_FORCEINLINE InsertResult Insert(TIn&& value)
	{
		InsertResult result = B::Insert(value, &GrowCallback<T, KeyTraits, THash,
			TAllocatorHandle, (TCapacity > 0)>, &static_cast<const TAllocatorHandle&>(B::m));

		if (result.Inserted)
		{
			Construct<T>(result.Element, std::forward<TIn>(value));
		}
		return result;
	}

	template<typename TIn>
	xGame_FORCEINLINE InsertResult InsertUnsafe(TIn&& value)
	{
		return B::Insert(value, &GrowCallback<T, KeyTraits, THash,
			TAllocatorHandle, (TCapacity > 0)>, &static_cast<const TAllocatorHandle&>(B::m));
	}


	template<typename TIn>
	xGame_FORCEINLINE bool Remove(const TIn& value)
	{
		T* element = B::Remove(value);
		if (element != nullptr)
		{
			Destroy(element);
			return true;
		}
		return false;
	}

	xGame_FORCEINLINE void Clear()
	{
		HashTable_::Clear<DestroyType<T>>(&B::m.core, sizeof(T));
	}


	xGame_FORCEINLINE void Reserve(iword minCapacity)
	{
		HashTable_::Reserve<T, KeyTraits,
			THash, TAllocatorHandle, (TCapacity > 0)>(
				&B::m.core, sizeof(T), minCapacity,
				static_cast<const TAllocatorHandle&>(B::m));
	}

	xGame_FORCEINLINE void ShrinkToFit();
};

#endif

} // namespace _::HashSet_


} // namespace xGame
*/
