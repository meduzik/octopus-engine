#pragma once
#include "allocator.hpp"
#include <octopus/meta/traits.hpp>

namespace octo {

template<class T>
struct OwnDeleter {
	void operator()(T* ptr) {
		Mem.destroy(ptr);
	}
};

template<class T>
using Own = std::unique_ptr<T, OwnDeleter<T>>;

namespace meta {

template<typename T>
constexpr bool IsTriviallyRelocatable<Own<T>> = true;

}


}
