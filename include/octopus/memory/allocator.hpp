#pragma once
#include <octopus/core/types.hpp>
#include <octopus/macros/assert.h>
#include <octopus/core/panic.h>
#include "memory.hpp"

namespace octo {

template<class Derived>
class Allocator {
public:
	u8* try_allocate(uword size) = delete;
	void free(u8* ptr, uword size) = delete;

	u8* allocate(uword size) {
		u8* p = ((Derived*)this)->try_allocate(size);
		if ((size != 0) & (!p)) {
			octo_panic("memory allocation failed");
		}
		return p;
	}

	u8* reallocate(u8* ptr, uword old_size, uword new_size) {
		u8* new_ptr = ((Derived*)this)->allocate(new_size);
		if (!new_ptr) {
			return nullptr;
		}
		memcpy(new_ptr, ptr, std::min(old_size, new_size));
		((Derived*)this)->free(ptr, old_size);
		return new_ptr;
	}

	template<class T, class... Args>
	T* create(Args&&... args) {
		return new(((Derived*)this)->allocate(sizeof(T))) T(std::forward<Args>(args)...);
	}

	template<class T>
	void destroy(T* object) {
		object->~T();
		((Derived*)this)->free((u8*)object, sizeof(T));
	}
};


template<class Alc>
struct AllocatorRef: public Allocator<AllocatorRef<Alc>> {
	u8* try_allocate(uword size) {
		return alc->try_allocate(size);
	}

	void free(u8* ptr, uword size) {
		alc->free(ptr, size);
	}

	AllocatorRef(Alc* alc) throw() : alc(alc) {}
private:
	Alc* alc;
};

template<class Alc, class T>
struct AllocatorStdRef : public std::allocator<T> {
	typedef size_t size_type;
	typedef T* pointer;
	typedef const T* const_pointer;

	template<typename _Tp1>
	struct rebind {
		typedef AllocatorStdRef<Alc, _Tp1> other;
	};

	pointer allocate(size_type n, const void* hint = 0) {
		return (pointer)alc->allocate(sizeof(T) * n);
	}

	void deallocate(pointer p, size_type n) {
		alc->free((u8*)p, sizeof(T) * n);
	}

	AllocatorStdRef(Alc* alc) throw() : alc(alc) {}
	AllocatorStdRef(const AllocatorStdRef<Alc, T>& a) throw() = default;
	AllocatorStdRef(AllocatorStdRef<Alc, T>&& a) throw() = default;
	template <class U>
	AllocatorStdRef(const AllocatorStdRef<Alc, U>& a) throw() : alc(a.alc) {}
	template <class U>
	AllocatorStdRef(AllocatorStdRef<Alc, U>&& a) throw() : alc(a.alc) {}
	~AllocatorStdRef() throw() = default;
private:
	template<class U, class V>
	friend struct AllocatorStdRef;

	Alc* alc;
};

template<class Alc, class T>
struct AllocatorStdWrapper : public std::allocator<T> {
	typedef size_t size_type;
	typedef T* pointer;
	typedef const T* const_pointer;

	template<typename _Tp1>
	struct rebind {
		typedef AllocatorStdWrapper<Alc, _Tp1> other;
	};

	pointer allocate(size_type n, const void *hint = 0) {
		return (pointer)alc.allocate(sizeof(T) * n);
	}

	void deallocate(pointer p, size_type n) {
		alc.free((u8*)p, sizeof(T) * n);
	}

	AllocatorStdWrapper() throw() : alc() {}
	AllocatorStdWrapper(Alc alc) throw() : alc(alc) {}
	AllocatorStdWrapper(const AllocatorStdWrapper<Alc, T> &a) throw() = default;
	AllocatorStdWrapper(AllocatorStdWrapper<Alc, T> &&a) throw() = default;
	template <class U>
	AllocatorStdWrapper(const AllocatorStdWrapper<Alc, U>& a) throw() : alc(a.alc) {}
	template <class U>
	AllocatorStdWrapper(AllocatorStdWrapper<Alc, U>&& a) throw() : alc(std::move(a.alc)) {}

	AllocatorStdWrapper<Alc, T>& operator=(AllocatorStdWrapper<Alc, T>&& other) = default;
	AllocatorStdWrapper<Alc, T>& operator=(const AllocatorStdWrapper<Alc, T>& other) = default;

	template <class U>
	AllocatorStdWrapper<Alc, T>& operator=(AllocatorStdWrapper<Alc,U>&& other) { alc = std::move(other.alc); return *this; }
	template <class U>
	AllocatorStdWrapper<Alc, T>& operator=(const AllocatorStdWrapper<Alc,U>& other) { alc = other.alc; return *this;  }
	~AllocatorStdWrapper() throw() = default;
private:
	template<class U, class V>
	friend struct AllocatorStdWrapper;

	Alc alc;
};


struct Mallocator: public Allocator<Mallocator> {
	u8* try_allocate(uword size) {
		return memory_allocate(size);
	}

	void free(u8* ptr, uword size) {
		memory_free(ptr, size);
	}
};

template<class Alc>
struct AlcRef : public Allocator<Alc> {
	AlcRef(Alc* ref): ref(ref) {
	}

	u8* try_allocate(uword size) {
		return ref->try_allocate(size);
	}

	void free(u8* ptr, uword size) {
		ref->free(ptr, size);
	}

	operator Alc*() const {
		return ref;
	}
private:
	Alc* ref;
};

struct DynamicAllocator {
	template<class Alc>
	static u8* wrap_allocate(void* alc_ptr, uword size) {
		Alc* alc = (Alc*)alc_ptr;
		return alc->allocate(size);
	}

	template<class Alc>
	static void wrap_free(void* alc_ptr, u8* ptr, uword size) {
		Alc* alc = (Alc*)alc_ptr;
		alc->free(ptr, size);
	}

	template<class Alc>
	DynamicAllocator(Alc& alc) :
		alc(&alc),
		fn_allocate(wrap_allocate<Alc>),
		fn_free(wrap_free<Alc>)
	{
	}

	u8* allocate(uword size) {
		return fn_allocate(alc, size);
	}

	void free(u8* ptr, uword size) {
		fn_free(alc, ptr, size);
	}
private:
	void* alc;
	u8* (*fn_allocate) (void* alc, uword size);
	void (*fn_free) (void* alc, u8* ptr, uword size);
};

inline Mallocator Mem;

}


