#pragma once

#include <octopus/macros/api.h>

namespace octo {

OCTO_PUBLIC
inline octo_forceinline u8* memory_allocate(uword size) {
	return (u8*)malloc(size);
}

OCTO_PUBLIC
inline octo_forceinline u8* memory_reallocate(u8* ptr, uword old_size, uword new_size) {
	return (u8*)realloc(ptr, new_size);
}

OCTO_PUBLIC
inline octo_forceinline void memory_free(u8* ptr, uword size) {
	free(ptr);
}



}
