#pragma once

#include "detect.h"


#if defined(OCTO_COMPILER_MSVC)
	#if OCTO_DEBUG
		#include <octopus/core/panic.h>
		#define octo_unreachable octo_panic("unreachable")
	#else
		#define octo_unreachable __assume(false)
	#endif
#elif defined(OCTO_COMPILER_CLANG)
	#if OCTO_DEBUG
		#include <octopus/core/panic.h>
		#define octo_unreachable octo_panic("unreachable")
	#else
		#define octo_unreachable __builtin_unreachable()
	#endif
#else
	#error Cannot infer compiler features
#endif
