#pragma once

#include "detect.h"

#if defined(OCTO_SHARED)
	#if defined(OCTO_BUILD)
		#if defined(OCTO_COMPILER_MSVC)
			#define OCTO_PUBLIC __declspec(dllexport) 
			#define OCTO_LOCAL
		#else
			#error Shared export decorator is not implemented for the compiler
		#endif
	#else
		#if defined(OCTO_COMPILER_MSVC)
			#define OCTO_PUBLIC __declspec(dllimport)
			#define OCTO_LOCAL
		#else
			#error Shared import decorator is not implemented for the compiler
		#endif
	#endif
#elif defined(OCTO_STATIC)
	#if defined(OCTO_BUILD)
		#if defined(OCTO_COMPILER_MSVC)
			#define OCTO_PUBLIC
			#define OCTO_LOCAL
		#elif defined(OCTO_COMPILER_CLANG)
			#define OCTO_PUBLIC
			#define OCTO_LOCAL
		#else
			#error Static export decorator is not implemented for the compiler
		#endif
	#else
		#if defined(OCTO_COMPILER_MSVC)
			#define OCTO_PUBLIC
			#define OCTO_LOCAL
		#elif defined(OCTO_COMPILER_CLANG)
			#define OCTO_PUBLIC
			#define OCTO_LOCAL
		#else
			#error Static import decorator is not implemented for the compiler
		#endif
	#endif
#else
	#error Neither OCTO_SHARED nor OCTO_STATIC are defined
#endif

// OCTO_NORETURN
// Declares that a function never returns

#if defined(__cplusplus)
	#define OCTO_C extern "C"
	#define OCTO_NORETURN [[noreturn]]
#else
	#define OCTO_C
	#if defined(OCTO_COMPILER_MSVC)
		#define OCTO_NORETURN __declspec(noreturn)
	#else
		#error Cannot infer OCTO_NORETURN macro for the compiler
	#endif
#endif

#if defined(OCTO_COMPILER_MSVC)
	#define OCTO_PRETTY_FUNCTION __FUNCSIG__

	#define octo_noinline __declspec(noinline)
	#define octo_forceinline __forceinline
	#define octo_unlikely(x) (x)
	#define octo_likely(x) (x)
#else
	#define OCTO_PRETTY_FUNCTION __PRETTY_FUNCTION__

	#define octo_noinline __attribute__((noinline))
	#define octo_forceinline __attribute__((always_inline))
	#define octo_unlikely(x) __builtin_expect(x, 0)
	#define octo_likely(x) __builtin_expect(x, 1)
#endif
