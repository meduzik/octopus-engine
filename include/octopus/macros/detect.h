#pragma once

#include "../config.h"

#if defined(__clang__)
	#define OCTO_COMPILER_CLANG
#elif defined(_MSC_VER)
	#define OCTO_COMPILER_MSVC
#else
	#error Cannot detect the compiler!
#endif

#if defined(_WIN64)
	#define OCTO_PLATFORM_WIN
	#define OCTO_PLATFORM_WIN64
	#define OCTO_PLATFORM_BIT 64
#elif defined(_WIN32)
	#define OCTO_PLATFORM_WIN
	#define OCTO_PLATFORM_WIN32
	#define OCTO_PLATFORM_BIT 32
#elif defined(__unix__)
	#if __x86_64__
		#define OCTO_PLATFORM_LINUX
		#define OCTO_PLATFORM_LINUX64
		#define OCTO_PLATFORM_BIT 64
	#else
		#define OCTO_PLATFORM_LINUX
		#define OCTO_PLATFORM_LINUX32
		#define OCTO_PLATFORM_BIT 32
	#endif
#else
	#error Cannot detect the platform!
#endif

#if defined(OCTO_SHARED) && defined(OCTO_STATIC)
	#error Both OCTO_SHARED and OCTO_STATIC are defined
#elif defined(OCTO_SHARED)

#elif defined(OCTO_STATIC)

#else
	#error You must define either OCTO_SHARED or OCTO_STATIC
#endif

#if !defined(OCTO_PLATFORM_BIT)
	#error OCTO_PLATFORM_BIT not defined
#elif OCTO_PLATFORM_BIT == 64
	#define OCTO_PLATFORM_64
#elif OCTO_PLATFORM_BIT == 32
	#define OCTO_PLATFORM_32
#else
	#error Invalid OCTO_PLATFORM_BIT
#endif

#if !defined(OCTO_DEBUG)
	#if defined(NDEBUG)
		#define OCTO_DEBUG 0
	#else
		#define OCTO_DEBUG 1
	#endif
#endif

#if !defined(OCTO_ASSERTS)
	#if OCTO_DEBUG
		#define OCTO_ASSERTS 1
	#else
		#define OCTO_ASSERTS 0
	#endif
#endif


