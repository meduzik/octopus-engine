#pragma once

#define OCTO_GET_MACRO_2(_1,_2,NAME,...) NAME
#define OCTO_EXPAND(x) x
#define OCTO_STRI(x) #x
#define OCTO_STR(x) OCTO_STRI(x)
#define OCTO_APPLY(f, args) f args


#define OCTO_BITFIELD_ENUM_OPS(Name) \
	inline octo_forceinline Name operator|(Name f1, Name f2) { return (Name)((uword)f1 | (uword)f2); } \
	inline octo_forceinline Name operator&(Name f1, Name f2) { return (Name)((uword)f1 & (uword)f2); } \
	inline octo_forceinline Name operator^(Name f1, Name f2) { return (Name)((uword)f1 ^ (uword)f2); } \
	inline octo_forceinline Name& operator|=(Name& f1, Name f2) { f1 = f1 | f2; return f1; } \
	inline octo_forceinline Name& operator&=(Name& f1, Name f2) { f1 = f1 & f2; return f1; } \
	inline octo_forceinline Name& operator^=(Name& f1, Name f2) { f1 = f1 ^ f2; return f1; } \
	inline octo_forceinline Name operator~(Name f) { return (Name)(~(uword)f); } \


#define OCTO_ZZ_BITFIELD_GEN_VALUE(Name, Value) Name = Value,
#define OCTO_ZZ_BITFIELD_GEN_FLAG(Name, Value) Name##Bit = ((u64)1 << (u64)Value),

#define OCTO_BITFIELD_ENUM(Name, ValueType, BitfieldType, X) \
	namespace Name { \
		enum Value: ValueType { \
			X(OCTO_ZZ_BITFIELD_GEN_VALUE) \
		}; \
		enum Flags: BitfieldType { \
			None = 0, \
			X(OCTO_ZZ_BITFIELD_GEN_FLAG) \
		}; \
		OCTO_BITFIELD_ENUM_OPS(Flags); \
	}