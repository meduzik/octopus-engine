#pragma once

#include "detect.h"
#include "api.h"
#include "control_flow.h"
#include "utils.h"

#if !defined(OCTO_ASSERTS)
	#error OCTO_ASSERTS must be defined
#endif

#if OCTO_ASSERTS == 1
#define ZZ_OCTO_ASSERT_2(condition, message) ( (condition) ? ((void)0) : octo_panic("assertion '" message "' failed") )


#elif OCTO_ASSERTS == 0
	#if defined(OCTO_COMPILER_MSVC)
		#define ZZ_OCTO_ASSERT_2(condition, message) __assume(condition)
	#else
		#define ZZ_OCTO_ASSERT_2(condition, message) (void)0
	#endif
#else
	#error OCTO_ASSERTS defined to an invalid value
#endif

#define ZZ_OCTO_ASSERT_1(condition) ZZ_OCTO_ASSERT_2(condition, #condition)

// checks the condition
// if the condition is false, then panics with the specified message, or
// the "assertion '" #cond "' failed" message
// if OCTO_ASSERTS==0, octo_assert is a noop
#define octo_assert(...) OCTO_EXPAND( OCTO_GET_MACRO_2(__VA_ARGS__, ZZ_OCTO_ASSERT_2, ZZ_OCTO_ASSERT_1)(__VA_ARGS__) )

#define octo_require(condition) ( (condition) ? ((void)0) : octo_panic("required '" #condition "'") )


