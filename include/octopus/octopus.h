#pragma once

#define OCTO_MCM_VERSION 1000000

#define OCTO_VERSION_MAJOR 0
#define OCTO_VERSION_MINOR 0
#define OCTO_VERSION_PATCH 0
#define OCTO_VERSION_PRERELEASE ""
#define OCTO_VERSION_BUILD ""
#define OCTO_VERSION_STRING "0.0.0"
#define OCTO_VERSION_STRING_FULL "0.0.0"

#include "macros/macros.h"
