#pragma once
#include <octopus/core/types.hpp>

namespace octo {

OCTO_PUBLIC
u64 timestamp_now();

OCTO_PUBLIC
u64 timestamp_appstart();


}
