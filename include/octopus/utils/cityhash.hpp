#pragma once

#include <octopus/core/types.hpp>


namespace octo {

struct CityHash {
	typedef u64 StateType;

	static constexpr u64 Seed = 0x600D5EED'BAADF00Dull;

	static octo_forceinline u64 Init() {
		return Seed;
	}

	template<typename T>
	static octo_forceinline u64 Combine(u64 state, T value) {
		static_assert(std::is_integral_v<T>);

		constexpr u64 k = 0x9ddfea08eb382d69;
		u64 m = (u64)(state + (u64)value);
		return m;
	}

	static octo_forceinline u64 Combine(
		u64 state,
		const void* data,
		uword size
	) {
		return Combine(state, CityHash64(data, size));
	}

	static octo_forceinline uword Finalize(u64 state) {
		return state;
	}

	static u64 CityHash64(const void* data, uword size);
};

}
