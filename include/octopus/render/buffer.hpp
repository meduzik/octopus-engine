#pragma once
#include "device.hpp"
#include "enums.hpp"

namespace octo::render {

struct Buffer;


OCTO_PUBLIC Buffer* buffer_create(Device* device, BufferUsage usage, BufferAccess access, uword size, const u8* data);
OCTO_PUBLIC void buffer_upload(Device* device, Buffer* buffer, uword offset, uword size, const u8* data);
OCTO_PUBLIC void buffer_destroy(Device* device, Buffer* buffer);

}
