#pragma once

namespace octo::render {

enum class TextureFormat {
	R8G8B8A8
};

enum class ShaderType {
	Vertex,
	Fragment
};

enum class PrimitiveType {
	Points,
	LineStrip,
	LineLoop,
	Lines,
	TrinalgeStrip,
	TriangleFan,
	Triangles
};

enum class VertexFormat {
	X32_Float,
	X32Y32_Float,
	X32Y32Z32_Float,
	X32Y32Z32W32_Float,
	
	X8Y8Z8W8_UNorm
};

enum class IndexType {
	U8,
	U16
};

enum class CompareOp {
	Never,
	Always,
	Equal,
	NotEqual,
	Less,
	Greater,
	LessEqual,
	GreaterEqual
};

enum class StencilOp {
	Keep,
	Zero,
	Replace,
	Incr,
	IncrWrap,
	Decr,
	DecrWrap,
	Invert
};

enum class CubeFace {
	PosX,
	NegX,
	PosY,
	NegY,
	PosZ,
	NegZ
};


#define X(a) \
	a(Red, 0) \
	a(Green, 1) \
	a(Blue, 2) \
	a(Alpha, 3) 
OCTO_BITFIELD_ENUM(ColorMask, u32, u32, X)
#undef X

enum BlendEquation {
	Add,
	Subtract,
	ReverseSubtract
};

enum class BlendFactor {
	One,
	Zero,
	SrcColor,
	OneMinusSrcColor,
	DstColor,
	OneMinusDstColor,
	SrcAlpha,
	OneMinusSrcAlpha,
	DstAlpha,
	OneMinusDstAlpha,
	RefColor,
	OneMinusRefColor,
	RefAlpha,
	OneMinusRefAlpha,
	SrcAlphaSaturate
};

enum class FaceOrientation {
	CW,
	CCW
};

enum class CullFace {
	None,
	Front,
	Back,
	All
};

enum class SamplerMinFilter {
	Nearest,
	Linear,
	NearestMipNearest,
	LinearMipLinear,
};

enum class SamplerMagFilter {
	Nearest,
	Linear
};

enum class SamplerWrap {
	Clamp,
	Repeat,
	RepeatMirror
};

#define X(V) \
	V(Viewport, 0) \
	V(Scissors, 1) \
	V(BlendConstant, 2) \
	V(StencilRef, 3) \
	V(StencilMask, 4)
OCTO_BITFIELD_ENUM(DynamicState, u32, u32, X);
#undef X

enum class BufferUsage {
	VertexData,
	IndexData
};

enum class BufferAccess {
	Static,
	Dynamic,
	Stream
};

}
