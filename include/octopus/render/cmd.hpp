#pragma once
#include "device.hpp"
#include "enums.hpp"
#include <octopus/math/rect.hpp>

namespace octo::render {

void cmd_begin_render(Device* device);

void cmd_viewport(Device* device, IRect rect, f32 z_near, f32 z_far);
void cmd_scissors(Device* device, IRect rect);

void cmd_bind_pipeline(Device* device, Pipeline* pipeline);

void cmd_stencil_ref(Device* device, u32 ref);
void cmd_stencil_mask(Device* device, u32 read_mask, u32 write_mask);

void cmd_blend_ref(Device* device, const f32* color);

void cmd_bind_sampler_view(Device* device, u32 location, SamplerView* sampler_view);

void cmd_uniform(Device* device, u32 location, u32 count, const i32* v);
void cmd_uniform(Device* device, u32 location, u32 count, const f32* v);
void cmd_uniform_matrix(Device* device, u32 location, u32 n, const f32* v);

void cmd_bind_vertex_buffer(Device* device, u32 binding, Buffer* buffer, uword offset);
void cmd_bind_index_buffer(Device* device, Buffer* buffer, uword offset, IndexType index_type);
void cmd_draw(Device* device, uword first_vertex, uword count);
void cmd_draw_indexed(Device* device, uword first_index, uword count);

void cmd_end_render(Device* device);

}
