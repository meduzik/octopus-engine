#pragma once
#include <octopus/memory/arena.hpp>
#include "device.hpp"
#include "enums.hpp"

namespace octo::render {

OCTO_PUBLIC Shader* shader_compile(Device* device, ShaderType type, StringRef source);
OCTO_PUBLIC void shader_destroy(Device* device, Shader* shader);
OCTO_PUBLIC bool shader_query_status(Device* device, Shader* shader, Arena* arena, StringRef* message);

}
