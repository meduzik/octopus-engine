#pragma once

namespace octo::render {

struct Device;
struct Buffer;
struct Texture2D;
struct TextureCube;
struct Pipeline;
struct Shader;
struct Program;
struct TextureHandle_t;
struct Sampler;
struct SamplerView;

using TextureHandle = TextureHandle_t*;


}

