#pragma once
#include "device.hpp"
#include "enums.hpp"

namespace octo::render {

struct Pipeline;

struct VertexDataSource {
	u32 index;
	u32 stride;
};

struct VertexAttrib {
	u32 location;
	u32 source;
	VertexFormat format;
	u32 offset;
};

struct VertexAttribInput {
	Span<VertexDataSource> sources;
	Span<VertexAttrib> attribs;
};

struct ShaderStages {
	Span< std::pair<StringRef, i32> > attribs;
	Span< std::pair<StringRef, i32> > uniforms;
	Span< std::pair<StringRef, i32> > samplers;
	Shader* vertex_shader;
	Shader* fragment_shader;
};

struct StencilFaceState {
	CompareOp op;
	u32 read_mask, write_mask;
	u32 reference;
	StencilOp fail, zfail, zpass;
};

struct DepthState {
	bool test;
	bool write;
	CompareOp op;
};

struct StencilState {
	bool test;
	StencilFaceState front, back;
};

struct BlendState {
	ColorMask::Flags color_mask;
	BlendEquation eq_color, eq_alpha;
	BlendFactor src_color, src_alpha;
	BlendFactor dst_color, dst_alpha;
	f32 ref_color[4];
};

struct PrimitiveState {
	PrimitiveType primitive_type;
	FaceOrientation front_face;
	CullFace cull_face;
	f32 line_width;
	bool depth_bias_enabled;
	f32 depth_bias_constant;
	f32 depth_bias_factor;
};

struct PipelineDescriptor {
	const ShaderStages* shaders;
	const VertexAttribInput* vertex_input;
	const StencilState* stencil;
	const DepthState* depth;
	const BlendState* blend;
	const PrimitiveState* primitive;
	DynamicState::Flags dynamic_state;
};

Pipeline* pipeline_create(Device* device, const PipelineDescriptor* descriptor);
void pipeline_destroy(Device* device, Pipeline* pipeline);

}

