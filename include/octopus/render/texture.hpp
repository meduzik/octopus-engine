#pragma once
#include "device.hpp"
#include "enums.hpp"

namespace octo::render {

struct Texture2D;
struct TextureCube;
struct TextureHandle_t;
struct Sampler;
using TextureHandle = TextureHandle_t*;

inline TextureHandle HandleOf(Texture2D* texture) { return (TextureHandle)texture; }
inline TextureHandle HandleOf(TextureCube* texture) { return (TextureHandle)texture; }

Texture2D* texture2d_create(Device* device, TextureFormat format, i32 width, i32 height);
void texture2d_upload_sync(Device* device, i32 level, const u8* data);
void texture2d_gen_mipmaps(Device* device, Texture2D* texture);
void texture2d_destroy(Device* device, Texture2D* texture);

TextureCube* texturecube_create(Device* device, TextureFormat format, i32 size);
void texturecube_upload_sync(Device* device, i32 level, CubeFace face, const u8* data);
void texturecube_gen_mipmaps(Device* device, Texture2D* texture);
void texturecube_destroy(Device* device, TextureCube* texture);

Sampler* sampler_create(Device* device, SamplerMinFilter min_filter, SamplerMagFilter mag_filter, SamplerWrap wrap_s, SamplerWrap wrap_t);
void sampler_destroy(Device* device, Sampler* sampler);

SamplerView* sampler_view_create(Device* device, Sampler* sampler, TextureHandle texture);
void sampler_view_destroy(Device* device, SamplerView* sampler_view);


}
