#pragma once

#include <octopus/macros/detect.h>
#include <octopus/core/types.hpp>

#if defined(OCTO_COMPILER_MSVC)
	#include <intrin.h>

namespace octo {

inline octo_forceinline u32 _ctz(u32 x) {
	unsigned long r = 0;
	_BitScanForward(&r, x);
	return r;
}

inline octo_forceinline u32 _clz(u32 value) {
	unsigned long r = 0;
	_BitScanReverse(&r, value);
	return 31 - r;
}

}

	#define octo_ctz32(x) octo::_ctz(x)
	#define octo_clz32(x) octo::_clz(x)
#else
	#include <x86intrin.h>
	#define octo_ctz32(x) __builtin_ctz(x)
	#define octo_clz32(x) __builtin_clz(x)
#endif


