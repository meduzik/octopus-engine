#pragma once

#include <octopus/core/types.hpp>
#include <octopus/macros/api.h>

namespace octo {

namespace comptime {


template<typename T>
constexpr octo_forceinline T round_up_to_pot(T value) {
	static_assert(std::is_unsigned_v<T>);
	T acc = 1;
	while (acc < value) {
		acc *= 2;
	}
	return acc;
}


}


#if defined(OCTO_COMPILER_MSVC)
	#include <intrin.h>

	inline octo_forceinline u32 intlog2(u32 val) {
		unsigned long r;
		_BitScanReverse(&r, val);
		return r;
	}

	inline octo_forceinline u64 intlog2(u64 val) {
	#if defined(OCTO_PLATFORM_WIN64)
		unsigned long r;
		_BitScanReverse64(&r, val);
		return r;
	#else
		u32 lo, hi;

		char high_set = _BitScanReverse(&hi, (u32)(val >> 32));
		_BitScanReverse(&lo, (tU32)val);
		hi |= 32;

		return high_set ? hi : lo;
	#endif
	}
#else
	inline octo_forceinline u32 intlog2(u32 val) {
		return 31 - __builtin_clz(val);
	}

	inline octo_forceinline u64 intlog2(u64 val) {
		return 63 - __builtin_clzll(val);
	}
#endif

}
