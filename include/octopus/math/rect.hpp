#pragma once

namespace octo {

template<class T>
struct Rect {
	T left, top, right, bottom;

	T get_width() const {
		return right - left;
	}

	T get_height() const {
		return bottom - top;
	}
};

using IRect = Rect<i32>;
using FRect = Rect<f32>;

}
