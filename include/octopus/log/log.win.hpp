#pragma once
#include "log.hpp"

namespace octo {

OCTO_PUBLIC ILogSink* log_create_win_console_sink();
OCTO_PUBLIC ILogSink* log_create_win_debug_output_sink();

}
