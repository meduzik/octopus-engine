#pragma once
#include <octopus/core/types.hpp>
#include <octopus/containers/twine.hpp>
#include <octopus/containers/vector.hpp>

namespace octo {


// Log entries are dispatched immediately, on the same thread where 
// they were created
#define OCTO_LOG_MODE_IMMEDIATE 1
// Log entries are buffered and dispatched when it is more convenient
#define OCTO_LOG_MODE_DELAYED 2


#if !defined(OCTO_LOG_MODE)
#define OCTO_LOG_MODE OCTO_LOG_MODE_IMMEDIATE
#endif


enum class LogLevel : u8 {
	// use for temporary debug output
	Debug = 1,
	// use for disabled-by-default output
	Verbose = 2,
	// use for general unimportant messages
	Info = 3,
	// use for something that should be addressed
	Warning = 4,
	// use for unexpectedly bad stuff
	Error = 5,
	// use for very bad stuff
	Fatal = 6
};

struct LogMessage {
	static constexpr uword DataInlineSize = 256;
	
	LogLevel level;
	u16 thread_id;
	u64 timestamp;
	const char* location;
	const char* function;
	const char* message;
	uword data_len;
	u8 data[];
};

struct OCTO_PUBLIC LogWriter {
	LogWriter(Arena& arena);
	~LogWriter();
	LogWriter(const Arena& arena) = delete;
	LogWriter(Arena&& arena) = delete;
	Arena& operator=(const Arena& arena) = delete;
	Arena& operator=(Arena&& arena) = delete;

	LogMessage* get_message() {
		return active_message;
	}

	enum class ValueTag: u8 {
		Null = 0,
		BoolFalse = 1,
		BoolTrue = 2,
		Int = 3,
		UInt = 4,
		Float = 5,
		Double = 6,
		String = 7,
		ByteString = 8,
		Array = 9,
		Dictionary = 10,
		Object = 11,
		EndMarker = 12,
	};

	void value(nullptr_t);
	template<class Bool>
	std::enable_if_t<std::is_same_v<Bool, bool>> value(bool value) {
		value_bool(value);
	}
	void value(u8 val);
	void value(u16 val);
	void value(u32 val);
	void value(i8 val);
	void value(i16 val);
	void value(i32 val);
	void value(u64 val);
	void value(i64 val);
	void value(f32 val);
	void value(f64 val);
	void key(StringRef val);
	void value(StringRef val);
	void key(const Twine& val);
	void value(const Twine& val);
	void array_begin();
	void array_end();
	void string_begin();
	void string_end();
	void bytestring_begin();
	void bytestring_end();
	void string_contents(const u8* data, uword len);
	void string_contents(StringRef val);
	void bytestring_contents(const u8* data, uword len);
	void dictionary_begin();
	void dictionary_end();
	void object_begin();
	void object_begin(StringRef name);
	void object_end();

	template<class Alc>
	LogMessage* allocate_message(Alc& alc) {
		uword size = (ptr - base) + sizeof(LogMessage);
		LogMessage* message = alc.allocate(size);
		memcpy(message, active_message, size);
		message->data_len = ptr - base;
		return message;
	}
private:
	void value_bool(bool val);

	u8* write(uword count);
	void store_skip_ref(u8* ptr);
	void resolve_skip_ref();

	u8* slow_write_grow(uword count);
	void select_message_container(LogMessage* message, uword data_size);

	u8* base;
	u8* ptr;
	u8* limit;
	Arena* arena;
	ArenaVector<u32>* skiprefs;
	LogMessage* active_message;

	alignas(LogMessage)
	u8 inline_message[sizeof(LogMessage) + LogMessage::DataInlineSize];
};

template<class T>
std::enable_if_t<std::is_same_v<
	decltype(std::declval<LogWriter&>().value(std::declval<T>())),
	void
>> write_object(LogWriter& writer, const T& val) {
	writer.value(val);
};


struct LogWritableVT {
	void (*write_object_fn) (void* object, LogWriter& writer);

	template<class T>
	inline static const LogWritableVT Instance;
};

template<class T>
inline const LogWritableVT LogWritableVT::Instance {
	[](void* object, LogWriter& writer) {
		write_object(writer, *(T*)object);
	}
};

struct LogValue {
	void* object;
	const LogWritableVT* vt;

	template<class T>
	LogValue(const T& val) :
		object((void*)&val),
		vt(&LogWritableVT::Instance<const T>) 
	{
	}
};

struct LogArg {
	StringRef name;
	LogValue value;

	LogArg(const char* name, LogValue value) :
		name(name),
		value(value)
	{
	}
};


class ILogSink {
public:
	virtual void accept_message(LogMessage* message) = 0;
};

OCTO_PUBLIC
ILogSink* log_get_stderr_sink();

OCTO_PUBLIC
StringRef log_format_string(Arena* arena, StringRef message, const u8* data);

OCTO_PUBLIC
void log_add_sink(ILogSink* sink);
OCTO_PUBLIC
void log_remove_sink(ILogSink* sink);

OCTO_PUBLIC octo_noinline
void log_report(LogLevel level, const char* location, const char* function, const char* text, std::initializer_list<LogArg> args);

#define OCTO_LOG_LOCATION __FILE__ ":" OCTO_STR(__LINE__)

#define LOG(level, message, ...) log_report(level, OCTO_LOG_LOCATION, OCTO_PRETTY_FUNCTION, message, {__VA_ARGS__})
#define LOGd(message, ...) LOG(LogLevel::Debug, message, __VA_ARGS__)
#define LOGv(message, ...) LOG(LogLevel::Verbose, message, __VA_ARGS__)
#define LOGi(message, ...) LOG(LogLevel::Info, message, __VA_ARGS__)
#define LOGw(message, ...) LOG(LogLevel::Warning, message, __VA_ARGS__)
#define LOGe(message, ...) LOG(LogLevel::Error, message, __VA_ARGS__)
#define LOGf(message, ...) LOG(LogLevel::Fatal, message, __VA_ARGS__)

}
