#pragma once

extern "C" {
	#include "octopus.h"
}

#include "core/core.hpp"
#include "macros/macros.h"
#include "containers/span.hpp"
#include "containers/string_ref.hpp"

namespace octo {

OCTO_PUBLIC int foo();

}

