#pragma once

#include <octopus/core/types.hpp>
#include <octopus/containers/string_ref.hpp>

namespace octo {

namespace format {

template<uword N>
struct FormatBuffer {
	static constexpr uword Size = N;

	operator StringRef() const {
		return StringRef(buffer, length);
	}

	u8 buffer[N];
	u16 length;
};

using IntegerBuffer = FormatBuffer<30>;

OCTO_PUBLIC IntegerBuffer decimal(i64 val);
OCTO_PUBLIC IntegerBuffer decimal(u64 val);

inline IntegerBuffer decimal(i8 val) { return decimal((i64)val); }
inline IntegerBuffer decimal(i16 val) { return decimal((i64)val); }
inline IntegerBuffer decimal(i32 val) { return decimal((i64)val); }
inline IntegerBuffer decimal(u8 val) { return decimal((u64)val); }
inline IntegerBuffer decimal(u16 val) { return decimal((u64)val); }
inline IntegerBuffer decimal(u32 val) { return decimal((u64)val); }


using FloatBuffer = FormatBuffer<63>;
using DoubleBuffer = FormatBuffer<383>;

OCTO_PUBLIC FloatBuffer scientific(float val);
OCTO_PUBLIC DoubleBuffer scientific(double val);
OCTO_PUBLIC FloatBuffer scientific(float val, uword precision);
OCTO_PUBLIC DoubleBuffer scientific(double val, uword precision);

OCTO_PUBLIC FloatBuffer fixed(float val);
OCTO_PUBLIC DoubleBuffer fixed(double val);
OCTO_PUBLIC FloatBuffer fixed(float val, uword precision);
OCTO_PUBLIC DoubleBuffer fixed(double val, uword precision);

OCTO_PUBLIC FloatBuffer shortest(float val, uword allowed_fixed_len = 6);
OCTO_PUBLIC DoubleBuffer shortest(double val, uword allowed_fixed_len = 6);


}

}
