#pragma once
#include <octopus/core/types.hpp>

namespace octo {

OCTO_PUBLIC
u16 thread_get_id();

}
