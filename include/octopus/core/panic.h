#pragma once

#include <octopus/macros/api.h>

typedef void(*octo_panic_callback)(void*, const char* file, int line, const char* function, const char* message);
typedef struct {
	void* ud;
	octo_panic_callback func;
} octo_panic_handler;
OCTO_C octo_panic_handler OCTO_PUBLIC octo_panic_handler_set(octo_panic_handler new_handler);
OCTO_C octo_panic_handler OCTO_PUBLIC octo_panic_handler_get();

OCTO_C OCTO_NORETURN
void OCTO_PUBLIC octo_raise_panic(const char* file, int line, const char* function, const char* message);

// prints the message and the current location, and terminates the program
#define octo_panic(message) (octo_raise_panic(__FILE__, __LINE__, __FUNCTION__, (message)), (void)0)

